# Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.
# You may assume that the array is non-empty and the majority element always exist in the array.
# Solving via Moore's voting
# on leet this is still 86% faster..think of someth
def majority_elem(nums):
    majElem = nums[0]
    count = 1

    for elem in nums[1:]:
        if elem == majElem:
            count+=1
        else:
            count-=1
            if count == 0:
                majElem = elem
                count = 1

#     If assumption that majority element is always there cannot be made
    if arr.count(majElem) < len(nums) / 2:
            print("Majority element does not exist")
    else:
            print(f"Majority element in {nums} is",majElem)

arr = [2,3,1]
majority_elem(arr)
arr = [2,3,2]
majority_elem(arr)
arr = [2,3,2]
majority_elem(arr)
arr = [3,3,3,4]
majority_elem(arr)
arr = [2,3,2,2,2,1,2,2,9,3]
majority_elem(arr)
