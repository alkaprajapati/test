# The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
# (you may want to display this pattern in a fixed font for better legibility)
# P   A   H   N
# A P L S I I G
# Y   I   R
# And then read line by line: "PAHNAPLSIIGYIR"
# Write the code that will take a string and make this conversion given a number of rows:
# string convert(string s, int numRows);
def zigZagConversion(s,numRows):
    arr = ['']*numRows
    lenN = len(s) - 1
    charIndex , col = 0, 0
    lastRow = numRows - 1

    if lenN <= 0 or numRows == 1:
            return s

    while charIndex <= lenN:
        if col % (lastRow) == 0:
            for r in range(0,numRows):
                if charIndex <= lenN:
                    arr[r] += s[charIndex]
                    charIndex += 1
        else:
            arr[lastRow - ( col % (lastRow) )] += s[charIndex]
            charIndex += 1
        col += 1

    s  = ''
    for row in range(0,numRows):
        s += arr[row]
    return s

# PAHNAPLSIIGYIR
print(zigZagConversion('PAYPALISHIRING',3))
# PINALSIGYAHRPI
print(zigZagConversion('PAYPALISHIRING',4))

