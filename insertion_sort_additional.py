# Insertion sort includes inserting A[i] (called key) into A[0:i-1] by pairwise sort down to the correct position
def insertion_sort(arr,len):

    for i in range (1,len):
        key = arr[i]
        j = i - 1

        # Keep shifting a[j] to a[j+1] till key is smaller than a[j]
        while j >= 0 and key < arr[j]:
            # 2345 key = 1
            arr[j+1] = arr[j]
            j -= 1

        # if the shift occurred than insert key to the last empty space
        # in case no shift then it just inserts key in the same place
        arr[j+1] = key

arr = [3,7,0,9,23,1,2,5,8,6]
insertion_sort(arr,10)
print(arr)

