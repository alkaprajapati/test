# Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:
# Integers in each row are sorted from left to right.
# The first integer of each row is greater than the last integer of the previous row.
# Binary Approach
def searchMatrix(matrix,target):

    start = 0
    end = len(matrix)-1

    if  end < 0: return False

    endCol = len(matrix[0])-1

    if endCol < 0: return False

    while start<=end:
        mid = (end + start) // 2
        print(start,end,mid)
        if target == matrix[mid][0] or target == matrix[mid][endCol] or ( target in matrix[mid]):
            return True
        elif target < matrix[mid][0]:
            end = mid-1
        else:
            start = mid+1
    return False
matrix=[[]]
print( searchMatrix(matrix,1) )
matrix=[[1]]
print( searchMatrix(matrix,5) )
matrix = [  [1,   3,  5,  7],  [10, 11, 16, 20],  [23, 30, 34, 50] ]
target = 3
print( searchMatrix(matrix,target) )
print( searchMatrix(matrix,10) )
print( searchMatrix(matrix,16) )
print( searchMatrix(matrix,20) )
print( searchMatrix(matrix,23) )
print( searchMatrix(matrix,30) )
print( searchMatrix(matrix,50) )
matrix = [  [1,   3,  5,  7],  [10, 11, 16, 20],  [23, 30, 34, 50] ]
target = 13
print( searchMatrix(matrix,target) )
