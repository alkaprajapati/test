# Find the number of ways to reach N from 0
# Given we can take a jump of 1,2 .... jump
# Notice that with recursion some cases are calculated more than once
# So in python we can use Dictionary to record all base cases, hence reduce the number of recursion calls
def staircase1(n,jump,baseCase):
    #Base
    # In this we not hardcoding the base case
    if n < 0:
        return 0
    elif n == 0 or n == 1:
        return 1
    elif n in baseCase:
        return baseCase[n]

    #Assumption + Induction step
    sum = 0
    for i in range(1,jump+1):
        baseCase[n-i] = staircase1(n-i,jump,baseCase)
        sum += baseCase[n-i]
    return sum

baseCase = {}
print(staircase1(9,4,baseCase))
print(baseCase)
baseCase = {}
print(staircase1(9,3,baseCase))
print(baseCase)
baseCase = {}
print(staircase1(9,2,baseCase))
print(baseCase)
baseCase = {}
print(staircase1(3,2,baseCase))
print(baseCase)
baseCase = {}
print(staircase1(3,3,baseCase))
print(baseCase)
