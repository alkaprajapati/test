# Convert string like '1234' to 1234
# ASCII Value of '0' -> 48, '1' -> 49 ...... '9'->48+9
def strToInteger(str,lenStr):
    # Base case return the empty case
    if lenStr == 0:
        return 0

    # Assumption + Induction Step
    pow      = 10 ** (lenStr-1)
    intDigit = ord(str[0]) - 48

    return intDigit*pow + strToInteger(str[1:],lenStr-1)

print(strToInteger('123',3))
print(strToInteger('4570',4))
print(strToInteger('40990',5))
