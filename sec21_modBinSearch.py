# Compute and return the square root of x, where x is guaranteed to be a non-negative integer.
# Only the integer part of the result is returned.
def sqrt(x):
    if  x == 0:
        return 0
    elif x == 1:
        return 1

    low = 1
    high = x
    ans = -1
    while low <= high:
        mid = (low + high) // 2
        if (mid*mid) == x:
            return mid
        # If left side
        elif x < (mid*mid):
            high = mid - 1
            ans = mid - 1
        # if Right side
        elif x > (mid*mid):
            low = mid + 1
            # ans = mid
    return ans

print('0:',sqrt(0))
print('1:',sqrt(1))
print('2:',sqrt(2))
print('4:',sqrt(4))
print('6:',sqrt(6))
print('8:',sqrt(8))
print('10:',sqrt(10))
print('16:',sqrt(16))
