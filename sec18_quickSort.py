# Quick Sort

def partitionArray(arr,start,end):
    pivot = arr[end]
    nextGreaterElem = start - 1

    for j in range(start,end):
        if arr[j] <= pivot:
            nextGreaterElem  = nextGreaterElem + 1
            (arr[nextGreaterElem],arr[j]) = (arr[j], arr[nextGreaterElem])

    # swap pivot and next greater elem
    (arr[nextGreaterElem+1],arr[end]) = (arr[end],arr[nextGreaterElem+1])
    return nextGreaterElem+1

def quickSort(arr,start,end):
    if start >= end:
        return
    pivot_index = partitionArray(arr,start,end)
    quickSort(arr,start,pivot_index-1)
    quickSort(arr,pivot_index+1,end)
    return

arr = [7,5,1,2,4,3]
quickSort(arr,0,5)
print(arr)
arr = [2,3,1,4,7,5,-2]
quickSort(arr,0,6)
print(arr)
arr = [2,4,7,3,5,9]
quickSort(arr,0,5)
print(arr)
