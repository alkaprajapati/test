# You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
# You may assume the two numbers do not contain any leading zero, except the number 0 itself.
# Example:
# Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
# Output: 7 -> 0 -> 8
# Explanation: 342 + 465 = 807.
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode) -> ListNode:

        if l1 == None:
            return l2
        if l2 == None:
            return l1

        digitFirstL = l1
        digitSecL   = l2
        carryOver = 0
        head = newDigitNode =  None

        # Loop through common n digits in both the linked List
        while digitFirstL != None and digitSecL != None:
            digitSum  = digitFirstL.val + digitSecL.val + carryOver
            carryOver = digitSum // 10
            digitSum  = digitSum % 10

            node = ListNode(digitSum, None)
            if newDigitNode == None:
                newDigitNode = node
                head = node
            else:
                head.next = node
                head = node
            digitFirstL = digitFirstL.next
            digitSecL= digitSecL.next

        # Loop through digits left in First Linked List
        while digitFirstL != None:
            digitSum  = digitFirstL.val + carryOver
            carryOver = digitSum // 10
            digitSum  = digitSum % 10

            node = ListNode(digitSum, None)
            if newDigitNode == None:
                newDigitNode = node
                head = node
            else:
                head.next = node
                head = node
            digitFirstL = digitFirstL.next

        # Loop through digits left in Sec Linked List
        while digitSecL != None:
            digitSum  = digitSecL.val + carryOver
            carryOver = digitSum // 10
            digitSum  = digitSum % 10

            node = ListNode(digitSum, None)
            if newDigitNode == None:
                newDigitNode = node
                head = node
            else:
                head.next = node
                head = node
            digitSecL = digitSecL.next

        # Check if CarryOver Left
        if carryOver != 0:
            node = ListNode(carryOver, None)
            if newDigitNode == None:
                newDigitNode = node
            else:
                head.next = node

        return newDigitNode
