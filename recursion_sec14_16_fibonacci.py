# The Fibonacci numbers, commonly denoted F(n) form a sequence, called the Fibonacci sequence,
# such that each number is the sum of the two preceding ones, starting from 0 and 1.
def recur_fibonacci(n):
    # Base Case
    if  n <= 1:
        return n
    else:
        # assumption + induction step
        return recur_fibonacci(n-1) + recur_fibonacci(n-2)

print(recur_fibonacci(0))
print(recur_fibonacci(1))
print(recur_fibonacci(2))
print(recur_fibonacci(3))
print(recur_fibonacci(4))
print(recur_fibonacci(5))
print(recur_fibonacci(10))
print(recur_fibonacci(11))
