# Print all Subsequent strings for a given string
# Include and exclude every character once from the current string
def subsequenceStringsArr(str,lenStr,out):
    if len(str) == 0:
        print(out)
        return
    for i in range(0,lenStr):
        char = str[i]
#       Exclude char
        subsequenceStringsArr(str[i+1:],lenStr-1,out)
# #       Include char
        subsequenceStringsArr(str[i+1:],lenStr-1,str[i]+out)

output = ''
subsequenceStringsArr('abc',3,output)
output = []
subsequenceStringsArr('abc',3,output)
