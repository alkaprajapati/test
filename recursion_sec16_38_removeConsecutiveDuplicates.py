# Remove consecutive duplicates from a string. E.g., 'aabc' -> 'abc'
# As String in python are immutable we will deploy slicing.
def removeDuplicates(str):
    # Base 0 1 2
    if len(str) <= 1:
        return str

    # Assumption + Induction Step
    if str[0] == str[1]:
        return removeDuplicates(str[1:])
    else:
        return str[0] + removeDuplicates(str[1:])

print(removeDuplicates(''))
print(removeDuplicates('b'))
print(removeDuplicates('abc'))
print(removeDuplicates('aaa'))
print(removeDuplicates('bccbbaay'))
print(removeDuplicates('abcdd'))
