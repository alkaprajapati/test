# def fibRec(n):
#     if n ==0 or n == 1:
# 	    return 1
#     return fibRec(n-1) + fibRec(n-2)
#
# # Time Complexity-> O(2**n)
#
# # 2.	Memorized Solution
# # We will create an array ARR of size n+1 where we are trying to find Fibonacci of n.
# # def helper(n,arr):
# #
# # 	if arr[n] != None:
# # 		return arr[n]
# #
# # 	if n == 0 or n == 1:
# #         arr[n] = 1
# #     else:
# #         arr[n] = helper(n-1,arr)+ helper(n-2,arr)
# #
# # 	return arr[n]
# #
# # def fibMemorize(n):
# #     arr = []*(n+1)
# #     helper(n.arr)
# # Complexity: O(no of calls to fib) = O(2n)
#
# # 3.	Bottom-Up
# def fibBottomUp(n):
# 	if n==1 or n ==0:
# 		return 1
# 	bottomUp = [None]*n
# 	bottomUp[0] = 1
#     bottomUp[1] = 1
#     for i in range(2,n+1):
# 	    bottomUp[i] = bottomUp[i-1]+ bottomUp[i-2]
# 	return bottomUp[n]
#
# # Complexity -> O(n)
