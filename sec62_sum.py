def sum1(nums,target):
    # Brute Force O(n**2)
    lenArr = len(nums)
    for i in range(lenArr-1):
        left = target - nums[i]
        for j in range(i+1,lenArr):
            if nums[j] == left:
                return [i,j]

nums = [2, 7, 11, 15]
target = 9
print(sum1(nums,target))

def sumhash(nums,target):
    # Hash Map O(n)
    hashMap = {}
    lenArr = len(nums)

    for i in range(lenArr):
        elem = target - nums[i]
        if elem in hashMap:
            return [hashMap[elem],i]
        else:
            hashMap[nums[i]] = i

nums = [2, 7, 11, 15]
target = 9
print(sumhash(nums,target))
