def lengthOfLastWord(s) :
    s = s.strip()
    a = s.split(" ")
    lastWord = a[len(a) -1]
    return len(lastWord)

print( lengthOfLastWord ( "Hello word" ) )
print( lengthOfLastWord ( "Hello word     " ) )
print( lengthOfLastWord ( "Hello wo rd " ) )
