# Remove duplicate from an array in linear time using hashMap
def removeDuplicates(arr):
    dict = {}

    for elem in arr:
        dict[elem] = dict.setdefault(elem,0)

    for elem in dict.keys():
        print(elem)

removeDuplicates([1,2,5,3,4,2,5])
removeDuplicates(['a','b','a'])
