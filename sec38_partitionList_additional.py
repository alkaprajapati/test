# Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.
# You should preserve the original relative order of the nodes in each of the two partitions.
# Input: head = 1->4->3->2->5->2, x = 3
# Output: 1->2->2->4->3->5
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def partition(self, head: ListNode, x: int) -> ListNode:
        before = after = False
        headBefore = beforeList = headAfter  = afterList = None

        while head!= None:
            if head.val < x:
                before = True
                if headBefore == None:
                    beforeList = headBefore = ListNode(head.val,None)
                else:
                    headBefore.next = ListNode(head.val,None)
                    headBefore = headBefore.next
            else:
                after = True
                if headAfter == None:
                    afterList = headAfter = ListNode(head.val,None)
                else:
                    headAfter.next = ListNode(head.val,None)
                    headAfter = headAfter.next
            head = head.next

        if after == False:
            return beforeList
        elif before == False:
            return afterList
        else:
            headBefore.next = afterList
            return beforeList
