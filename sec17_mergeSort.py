def mergeArray(arr,start,end,mid):
    # we are using two different arrays to store both halves
    # We using the actual array with the mentioned start and end
    left  = arr[start:mid+1]
    right = arr[mid+1:end+1]

    # do this to avoid adding the remaining parts of the bigger array
    left.append(float('inf'))
    right.append(float('inf'))

    i,j = 0,0
    # Compare the element of both halves
    for k in range(start,end+1):
        if left[i] < right[j]:
            arr[k] = left[i]
            i+=1
        else:
            arr[k] = right[j]
            j+=1

def mergeSort(arr,start,end):
    if start >= end:
        return

    mid = ( start + end ) // 2

    # Sort First half of the array
    mergeSort(arr,start,mid)
    # Sort second half of the array
    mergeSort(arr,mid+1,end)
    # Merge the sorted arrays
    mergeArray(arr,start,end,mid)

    return

arr = [7,5,1,2,4,3,41,23,6,8,33]
mergeSort(arr,0,10)
print(arr)

arr = [7,5,1,2,1,3]
mergeSort(arr,0,5)
print(arr)

arr = [7,5,1,2,4,3]
mergeSort(arr,0,5)
print(arr)
