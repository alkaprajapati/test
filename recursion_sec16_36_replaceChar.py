# Replace a character with another in a string
# As String in python are immutable wr are not replacing char at any index.
# In one approach we can simply build a new string
# In another approach we send string as a list
def replaceChar(str,oldChar,newChar):
    # Base
    if str == '':
        return ''

    if str[0] == oldChar:
        return newChar + replaceChar(str[1:],oldChar,newChar)
    else:
        return str[0] + replaceChar(str[1:],oldChar,newChar)

# If string converted to a list
def replaceChar1(str,oldChar,newChar):
    # Base
    if str == []:
        return

    if str[0] == oldChar:
        str[0] = newChar

    return replaceChar1(str[1:],oldChar,newChar)

print(replaceChar('','a','z'))
print(replaceChar('abc','a','z'))
print(replaceChar('bcdbaay','b','y'))

str = list(['a','b','c'])
replaceChar1(str,'a','z')
print(str)
