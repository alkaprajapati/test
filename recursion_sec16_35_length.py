# Find the length of an array
def length(arr):
    # Base
    if arr == [] or arr == '':
        return 0

    return 1+length(arr[1:])

print(length([]))
print(length([1]))
print(length([1,2]))
print(length([1,2,3]))
print(length(''))
print(length('abc'))
print(length('abcde'))

