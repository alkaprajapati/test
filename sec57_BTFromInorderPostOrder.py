# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def buildTree(self, inorder: List[int], postorder: List[int]) -> TreeNode:
        nodes = len(inorder) - 1
        return self.buildTreehelper(inorder,postorder,0,nodes,0,nodes)

    def buildTreehelper(self, ino, post, inS, inE, postS, postE):

        if inS > inE: return None
        rootVal   = post[postE]

        print(rootVal,postS,inS,inE)
        rootIdx  = None
        found    = False

        # Root Index
        for i in range(inS,inE+1):
            if ino[i] == rootVal:
                rootIdx = i
                found = True
                break

        if found == False: return None

        leftinS    = inS
        leftinE    = rootIdx -1
        rightinS   = rootIdx + 1
        rightinE   = inE

        leftpostS  = postS
        leftpostE  = leftpostS + leftinE - leftinS
        rightpostS = leftpostE + 1
        rightpostE = postE - 1

        root = TreeNode(rootVal)
        print("left",leftinS,leftinE,leftpostS,leftpostE)
        print("right",rightinS,rightinE,rightpostS,rightpostE)
        root.left  = self.buildTreehelper(ino,post,leftinS,leftinE,leftpostS,leftpostE)
        root.right = self.buildTreehelper(ino,post,rightinS,rightinE,rightpostS,rightpostE)
        return root
