# Print the count of occurrence of an element.
# In this approach we are passing a variable. Pass this by reference and print the var.
# Or pass by value and print inside the recursion method
# In python, we have ‘pass by object reference.’ As Integer is immutable, a workaround is to wrap it as list.
def countOccurrence(arr,arrLen,elem,count):
    # End -> Start
    # Base
    if arrLen == 0:
        return

    # Assumption + Induction Step
    if elem == arr[arrLen-1]:
        count[0] = count[0] + 1
    # print(count)
    return countOccurrence(arr,arrLen-1,elem,count)

print("Case1:")
count = [0]
countOccurrence([1,2,3,4,5],5,4,count)
print(count[0])

print("Case2:")
count = [0]
countOccurrence([1,4,3,4,5],5,4,count)
print(count[0])

print("Case3:")
count = [0]
countOccurrence([1,0,3,0,0],5,0,count)
print(count[0])

print("Case4:")
count = [0]
countOccurrence([1,2,5,5,5],5,9,count)
print(count[0])
