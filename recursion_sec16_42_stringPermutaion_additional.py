# Convert string like '1234' to 1234
# ASCII Value of '0' -> 48, '1' -> 49 ...... '9'->48+9
def stringPermutationAdditional(str,lenStr):
    # Base case return the empty case
    if lenStr == 1:
        return [str[0]]
    newOut = []
    # Assumption + Induction Step
    i = 0
    for i in range(0,lenStr):
        str1 =  str[:]
        fixed = str1.pop(i)
        # print(str1,str,i,fixed,lenStr-1)
        prevStr = stringPermutationAdditional(str1,lenStr-1)
        # print("out is",prevStr)
        for elem in prevStr:
            newOut.append(fixed+elem)
    return newOut

print( stringPermutationAdditional(list('abc'),3) )
print( stringPermutationAdditional(list('abcd'),4) )
