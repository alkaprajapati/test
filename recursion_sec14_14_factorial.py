# Recursion is serious magic :) loving it
def recur_factorial(n):
    if n < 0:
        return -1
    elif n == 1 or n == 0:
        return 1
    else:
        return n * recur_factorial(n-1)

print(recur_factorial(1))
print(recur_factorial(2))
print(recur_factorial(3))
print(recur_factorial(4))
print(recur_factorial(10))
print(recur_factorial(-10))
