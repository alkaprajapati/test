# there are two ways to create initial array
# METHOD 1
print('----Method 1----')
rows, cols = (5, 5)
arr1 = [[0]*cols]*rows
print(arr1)
# METHOD 2
print('----Method 2----')
rows, cols = (5, 5)
arr2 = [[0 for i in range(cols)] for j in range(rows)]
print(arr2)

# But there's a difference in these two methods:
arr1[0][1] = 2
arr2[0][1] = 2
print(arr1)
print(arr2)
# Method 2 worked as expected, only the [i][j] component is affected, in method 1 all [i][0...n] components are affected
# REASON:

# SLICLING IN 2D ARRAY
print('----arr slicing 2d----')
print('ARR[:][range] will work')
arr = [[1,2],[3,4]]
print(arr[:][0:2])
print('But ARR[range][:] will not work in all cases')
print("nope1",arr[0:1][0])
print("nope",arr[0:2][1])
# Hence, use
print([arr[i][0] for i in range(2)])
