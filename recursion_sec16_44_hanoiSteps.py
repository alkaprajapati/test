def hanoiSteps(n):
    #Base
    if n == 0:
        return 0

    # Assumption + Induction step
    #  Steps to transfer n-1 disks from Original to Helper tower
    #  1 step to transfer the nth disk from Original to Destination tower
    # Steps to transfer the n-1 disk from Helper to Destination tower
    return  (2 * hanoiSteps(n-1) ) + 1

print(hanoiSteps(0))
# for n disk, tower of hanoi takes 2**n - 1 steps
print(hanoiSteps(1),2**1 -1)
print(hanoiSteps(2),2**2 -1)
print(hanoiSteps(3),2**3 -1)
print(hanoiSteps(4),2**4 -1)
print(hanoiSteps(5),2**5 -1)
print(hanoiSteps(6),2**6 -1)
print(hanoiSteps(7),2**7 -1)
