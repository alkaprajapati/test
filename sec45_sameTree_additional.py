# Given two binary trees, write a function to check if they are the same or not.
# Two binary trees are considered the same if they are structurally identical and the nodes have the same value.
# Example 1:
# Input:     1         1
#           / \       / \
#          2   3     2   3
#         [1,2,3],   [1,2,3]
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isSameTree(self, p: TreeNode, q: TreeNode) -> bool:
        # Tried with recursion
        # O(height of longer tree)
        if p == None and q == None:
            return True

        if (p == None or q == None) or ( p.val != q.val ):
            return False

        ans = self.isSameTree(p.left,q.left)
        ans = ans and self.isSameTree(p.right,q.right)
        return ans
