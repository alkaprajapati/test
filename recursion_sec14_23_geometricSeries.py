# given k = 3 find geometric series with base 2
#  1 + 1/2**1 + 1/2**2 + 1/2**3
def geometricSeries(n):
    # Base
    if  n == 0:
        return 1
    # Assumption + Induction Step
    prevSeries = geometricSeries(n-1)
    return 1 / (2**n) + prevSeries

print(geometricSeries(0))
print(geometricSeries(1))
print(geometricSeries(2))
print(geometricSeries(3))

