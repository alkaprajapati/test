def fizzBuzz(n):
    list = []
    for i in range(1,n+1):
        if i % 3 == 0 and i % 5 == 0:
            list.append('FizzBuzz')
        elif i % 3 == 0:
            list.append('Fizz')
        elif i % 5 == 0:
            list.append('Buzz')
        else:
            list.append(str(i))
    return list
# Note to self: I am seeing immense diff when '' instead of "" is used. Even though there is no documentation of this anywhere
# I believe the peroformance gain is becuase with "" complier checks the content as any of it can represent a variable.
# same as selecting ball from bucket vs selecting a colored ball
print(fizzBuzz(15))
