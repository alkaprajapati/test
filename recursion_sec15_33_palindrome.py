# Check if string is a palindrome
import re
def palindrome(str,start,end):
    # Base
    if start > end:
        return True
    # Assumption + Induction Step
    if str[start] == str[end]:
        return palindrome(str,start+1,end-1)
    else:
        return False

s = 'race a car'
s = re.sub('[^a-zA-Z0-9]','',s)
s = s.lower()
print(f"Is {s} a palindrome? ",palindrome(s,0,len(s)-1))
s='race e car'
s = re.sub('[^a-zA-Z0-9]','',s)
s = s.lower()
print(f"Is {s} a palindrome? ",palindrome(s,0,len(s)-1))
s='A man, a plan, a canal: Panama'
s = re.sub('[^a-zA-Z0-9]','',s)
s = s.lower()
print(f"Is {s} a palindrome? ",palindrome(s,0,len(s)-1))
s='0P'
s = re.sub('[^a-zA-Z0-9]','',s)
s = s.lower()
print(f"Is {s} a palindrome? ",palindrome(s,0,len(s)-1))
s=[1,2,3,4,3,2,1]
print(f"Is {s} a palindrome? ",palindrome(s,0,len(s)-1))
s=[1,2,3,3,2,1]
print(f"Is {s} a palindrome? ",palindrome(s,0,len(s)-1))
s=[1,2,5,1]
print(f"Is {s} a palindrome? ",palindrome(s,0,len(s)-1))
