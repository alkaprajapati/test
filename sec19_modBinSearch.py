# Given an array of integers nums sorted in ascending order, find the starting and ending position of a given target value.
def firstIndex(nums, target):
    low = 0
    high = len(nums) - 1
    result = -1
    while low <= high:
        mid = ( high + low ) // 2
        if   nums[mid] == target:
            result = mid
            if mid-1 > 0 and nums[mid-1] != target:
                return result
        if nums[mid] >= target:
            high = mid - 1
        elif nums[mid] < target:
            low = mid + 1
    return result

def lastIndex(nums, target):
    low = 0
    high = len(nums) - 1
    result = -1
    while low <= high:
        mid = ( high + low ) // 2
        if   nums[mid] == target:
            result = mid
            if mid+1 <= high and nums[mid+1] != target:
                return result
        if nums[mid] > target:
            high = mid - 1
        elif nums[mid] <= target:
            low = mid + 1
    return result

def getFirstLastIndex(nums, target):
    if  nums == []:
        return [-1,-1]

    first = firstIndex(nums, target)
    last  = lastIndex(nums, target)
    return [first,last]

arr = []
print(getFirstLastIndex(arr,2))
arr = [1]
print(getFirstLastIndex(arr,1))
arr = [1,2,3,4,5]
print(getFirstLastIndex(arr,2))
arr = [1,2,3,4,5]
print(getFirstLastIndex(arr,4))
arr = [1,2,3,4,5]
print(getFirstLastIndex(arr,3))
arr = [1,3,3,4,5]
print(getFirstLastIndex(arr,3))
arr = [1,2,3,3,5]
print(getFirstLastIndex(arr,3))
arr = [1,3,3,3,5]
print(getFirstLastIndex(arr,3))
arr = [3,3,3,3,3]
print(getFirstLastIndex(arr,3))
arr = [1,2,3,4,5,5,5]
print(getFirstLastIndex(arr,5))
arr = [5,7,7,8,8,10]
print(getFirstLastIndex(arr,8))
