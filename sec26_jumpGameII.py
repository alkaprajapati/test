# Given an array of non-negative integers, you are initially positioned at the first index of the array.
# Each element in the array represents your maximum jump length at that position.
# Your goal is to reach the last index in the minimum number of jumps.
# You can assume that you can always reach the last index.
def jumpGame(nums):
    n = len(nums)
    if n == 1:
        return 0
    if n == 0 or nums[0] == 0 :
        return -1
    maxReachableIndex = steps = nums[0]
    jump = 1

    for i in range(1,n-1):
        currReachableIndex = i + nums[i]
        maxReachableIndex = max(currReachableIndex,maxReachableIndex)
        steps -= 1
        if steps == 0:
            # if i < n-1:
            jump += 1
            steps = maxReachableIndex - i
            if steps <= 0:
                return -1

    return jump
nums = [5,9,3,2,1,0,2,3,3,1,0,0]
print(jumpGame(nums))

# nums=[]
# print(jumpGame(nums))
# nums=[1]
# # print(jumpGame(nums))
# nums=[2,1]
# print(jumpGame(nums))
# # nums=[3,2,1,0,4]
# # print(jumpGame(nums))
# nums=[3,6,1,0,4]
# print(jumpGame(nums))
# nums=[3,3,1,0,2,1,0,4
# print(jumpGame(nums))
# nums=[3,3,1,1,0]
# print(jumpGame(nums))
# nums=[2,3,1,1,2,4,2,0,1,1]
# print(jumpGame(nums))
nums = [7,0,9,6,9,6,1,7,9,0,1,2,9,0,3]
print(jumpGame(nums))
# nums = [7,0,9,6,9,6,1,9,9,0,1,2,9,0,3]
# print(jumpGame(nums))
# nums = [7,0,9,6,9,6,1,1,9,0,1,2,9,0,3]
# print(jumpGame(nums))
