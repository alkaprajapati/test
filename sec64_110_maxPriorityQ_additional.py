class MaxPriorityQueue:

    def __init__(self):
        self.pq = []
        self.pqLen = 0
        pass

    def getParent(self,childIndex):
        parentIndex = (childIndex-1) // 2
        if parentIndex >= 0:
            return parentIndex
        return None

    def getMaxChild(self,parentIdx):
        leftChildIdx = ( (2 * parentIdx) + 1)
        rightChildIdx = leftChildIdx+1
        if leftChildIdx >= self.pqLen and  rightChildIdx >= self.pqLen:
            return None
        elif leftChildIdx >= self.pqLen:
            return rightChildIdx
        elif rightChildIdx >= self.pqLen:
            return leftChildIdx
        else:
            if self.pq[leftChildIdx] >= self.pq[rightChildIdx]:
                return leftChildIdx
            else:
                return rightChildIdx

    def upHeapify(self,childIdx,pq):
        while childIdx >= 0:
            parentIdx = self.getParent(childIdx)
            if parentIdx == None: return
            if pq[childIdx] > pq[parentIdx]:
                (pq[parentIdx],pq[childIdx]) = (pq[childIdx],pq[parentIdx])
                childIdx    = parentIdx
            else:
                return

    def downHeapify(self,parentIdx,pq):
        while parentIdx < self.pqLen:
            childIdx = self.getMaxChild(parentIdx)
            if childIdx == None: return

            if pq[parentIdx] < pq[childIdx]:
                (pq[parentIdx],pq[childIdx]) = (pq[childIdx],pq[parentIdx])
                parentIdx    = childIdx
            else:
                return

    def insert(self,elem):
        self.pq.append(elem)
        self.childIdx = self.pqLen
        self.pqLen += 1
        self.upHeapify(self.childIdx,self.pq)

    def getMax(self):
        if not self.isEmpty():
            return self.pq[0]
        return  None

    def isEmpty(self):
        if  self.pqLen == 0:
            return True
        return False

    def getSize(self):
        return self.pqLen

    def removeMax(self):
        (self.pq[0],self.pq[self.pqLen-1]) = (self.pq[self.pqLen-1],self.pq[0])
        poppedElem = self.pq.pop()
        self.pqLen-=1
        self.downHeapify(0,self.pq)
        return poppedElem
# arr= [12,6,5,100,1,9,0,14]
# maxHeap = MaxPriorityQueue()
# maxHeap.insert(12)
# maxHeap.insert(6)
# print(maxHeap.pq)
# maxHeap.insert(5)
# print(maxHeap.pq)
# maxHeap.insert(100)
# print(maxHeap.pq)
# maxHeap.insert(1)
# print(maxHeap.pq)
# maxHeap.insert(9)
# print(maxHeap.pq)
# maxHeap.insert(0)
# print(maxHeap.pq)
# maxHeap.insert(14)
# print(maxHeap.pq)
# print(maxHeap.getSize())
# print(maxHeap.isEmpty())
# maxHeap.removeMax()
# print(maxHeap.pq)
# maxHeap.removeMax()
# print(maxHeap.pq)
# maxHeap.insert(4)
# print(maxHeap.pq)
# maxHeap.insert(10)
# print(maxHeap.pq)
