# Find first and last index of an element in a sorted array
def modBinarySearch(arr,searchElem,start,end):
    if start > end:
        return [-1]

    mid = ( end + start ) // 2

    if arr[mid] == searchElem:
        first, last = -1, -1
        if mid-1 >= start and arr[mid-1] == searchElem:
            first = modBinarySearch(arr,searchElem,start,mid-1)

        if mid+1 <= end and arr[mid+1] == searchElem:
            last = modBinarySearch(arr,searchElem,mid+1,end)

        if first == -1 and last == -1:
            return [mid,mid]
        else:
            if first != -1 and last != -1:
                return [first[0],last.pop()]
            elif first != -1:
                return [first[0],mid]
            elif last != -1:
                return [mid,last.pop()]

    elif arr[mid] < searchElem:
        return modBinarySearch(arr,searchElem,mid+1,end)
    elif arr[mid] > searchElem:
        return modBinarySearch(arr,searchElem,start,mid-1)

arr = [1,2,3,4,5]
print(modBinarySearch(arr,2,0,4))
arr = [1,2,3,4,5]
print(modBinarySearch(arr,4,0,4))
arr = [1,2,3,4,5]
print(modBinarySearch(arr,3,0,4))
arr = [1,3,3,4,5]
print(modBinarySearch(arr,3,0,4))
arr = [1,2,3,3,5]
print(modBinarySearch(arr,3,0,4))
arr = [1,3,3,3,5]
print(modBinarySearch(arr,3,0,4))
arr = [3,3,3,3,3]
print(modBinarySearch(arr,3,0,4))
arr = [1,2,3,4,5,5,5]
print(modBinarySearch(arr,5,0,6))
