# Given a non-empty array of integers, every element appears twice except for one. Find that single one.
# Input: [2,2,1]        Output: 1
# Input: [4,1,2,1,2]    Output: 4
def singleNumberSort(nums):
    # 89% faster
    # T(nlogn)S(n)
    if nums == []:
        return
    elif len(nums) == 1:
        return nums[0]
    lenN = len(nums)
    nums.sort()
    i = 0
    while i < lenN:
        elem = nums[i]
        if  elem == nums[(i+1)%lenN]:
            i += 2
        else:
            return elem

def singleNumberHash(nums):
    # 89% faster
    # T(n)S(n)
    if nums == []:
        return
    elif len(nums) == 1:
        return nums[0]

    charCount = {}
    for i in range(0,len(nums)):
        elem = nums[i]
        if elem not in charCount:
            charCount[elem] = 1
        else:
            charCount[elem] +=1

    for char,count in charCount.items():
        if count == 1:
            return char

def singleNumberBit(nums):
    # Best approach T(n) S(1) 95% faster
    # Wohaaa I love this
    # XOR -> Bitwise operator that compares the binaries
    # return 1 only if the bits are different, else 0
    # so for two same values XOR is always 0, and XOR of elem with 0 will be the element itself
    #  in the array only one value is single, rest appear twice
    # if we XOR all value, we should be left with the unique value :)
    sum = 0
    for elem in nums:
        sum ^= elem
    return sum

print("---Sort & Search T(nlogn) S(1)---")
nums = [4,1,2,1,2]
print(singleNumberSort(nums))
nums = [4,1,2,1,2,4,3]
print(singleNumberSort(nums))
nums = [4,1,2,1,2,4,3,0,3]
print(singleNumberSort(nums))
print("---Bit wise operator---")
print("---hash T(n) S(n)---")
nums = [4,1,2,1,2]
print(singleNumberHash(nums))
nums = [4,1,2,1,2,4,3]
print(singleNumberHash(nums))
nums = [4,1,2,1,2,4,3,0,3]
print(singleNumberHash(nums))
print("---Bit wise operator T(n) S(1)---")
nums = [4,1,2,1,2]
print(singleNumberBit(nums))
nums = [4,1,2,1,2,4,3]
print(singleNumberBit(nums))
nums = [4,1,2,1,2,4,3,0,3]
print(singleNumberBit(nums))
