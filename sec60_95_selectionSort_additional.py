def selectionSort(arr):

    lenArr = len(arr)

    for i in range(lenArr-1):

        smallestElemIdx = i
        for j in range(i+1,lenArr):

            if arr[j] <  arr[smallestElemIdx]:
                smallestElemIdx = j

        if arr[i] > arr[smallestElemIdx]:
            (arr[i],arr[smallestElemIdx]) = (arr[smallestElemIdx],arr[i])

    return arr

print(selectionSort([1,4,2,4,6,8,7,1]))
print(selectionSort([7,5,3,6,8]))
print(selectionSort([7]))
print(selectionSort([7,1]))
