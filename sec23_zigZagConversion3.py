# The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
# (you may want to display this pattern in a fixed font for better legibility)
# P   A   H   N
# A P L S I I G
# Y   I   R
# And then read line by line: "PAHNAPLSIIGYIR"
# Write the code that will take a string and make this conversion given a number of rows:
# string convert(string s, int numRows);
def zigZagConversion(s,numRows):
    arr = [''] * numRows
    count =  0
    direction = 1

    for char in s:
        if  direction == 1 and count >= numRows:
            direction = -1
            count = numRows - 2
        if direction == -1 and count <= 0:
            direction = 1
            count = 0
        print(arr[count])
        arr[count] += char
        print("-",arr[count])
        if direction == 1:
            count+=1
        else:
            count-=1

    return ''.join(arr)

print(zigZagConversion("ABCD",2))
# print(zigZagConversion('PAYPALISHIRING',1))
# print(zigZagConversion('PAY',3))
# print(zigZagConversion('PA',3))
# print(zigZagConversion('',3))
# # PAHNAPLSIIGYIR
# print(zigZagConversion('PAYPALISHIRING',3))
# # PINALSIGYAHRPI
# print(zigZagConversion('PAYPALISHIRING',4))
#
