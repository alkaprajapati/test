# Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
# An input string is valid if:
# Open brackets must be closed by the same type of brackets.
# Open brackets must be closed in the correct order.
# Note that an empty string is also considered valid.
def validParenthesis(s):
    lastOpen = []

    for char in s:
        if char in ['(','{','[']:
            lastOpen.append(char)
        else:
            if len(lastOpen) > 0:
                last = lastOpen.pop()
            else:
                return False
            if not ((last == '(' and char == ')') or (last == '{' and char == '}') or (last == '[' and char == ']') ):
                return False
    if len(lastOpen) > 0:
        return False
    return True
