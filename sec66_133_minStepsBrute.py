def minStepsBrute(n):
    if n <= 1:
        return 0

    # decrement by 1
    opt1 = minStepsBrute(n-1)
    # Divide by 2
    opt2 = float('inf')
    if n % 2 == 0:
        opt2 = minStepsBrute(n/2)
    # Divide by 3
    opt3 = float('inf')
    if n % 3 == 0:
        opt3 = minStepsBrute(n/3)

    return min(opt1,opt2,opt3)+1

print(minStepsBrute(7))
print(minStepsBrute(11))
