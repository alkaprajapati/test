# Given a string S of lowercase letters, a duplicate removal consists of choosing two adjacent and equal letters, and removing them.
# We repeatedly make duplicate removals on S until we no longer can.
# Return the final string after all such duplicate removals have been made.  It is guaranteed the answer is unique.
def remAdjacentDuplicates(s,k):
    # TODO: Needs improvement
    # 57.3% T(n) O(n)
    stack = []
    for ch in s:

        if len(stack)==0:
            stack.append([ch,1])
            continue

        chTop   = stack[-1][0]
        chCount = stack[-1][1]

        if  chTop!= ch:
            stack.append([ch,1])
        else:
            chCount +=1
            if chCount < k:
                print("here")
                stack.pop()
                stack.append([ch,chCount])
            else:
                stack.pop()
    s = ''
    print(stack)
    for i,j in stack:
        s += i*j

    print(s)
    return s

remAdjacentDuplicates('saasa',2)
remAdjacentDuplicates('saaassa',3)
