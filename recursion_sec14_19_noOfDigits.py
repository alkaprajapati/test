# Count number of digits in a number
def noOfDigits(n):
    # Base
    if  n == 0:
        return 0

    # Assumption + Induction Step
    return 1 + noOfDigits(n//10)

print(noOfDigits(9))
print(noOfDigits(12))
print(noOfDigits(100))
print(noOfDigits(143))
print(noOfDigits(1435))
print(noOfDigits(14355))
