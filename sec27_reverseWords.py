# Given an input string, reverse the string word by word.
# Input: "the sky is   blue  "
# Output: "blue is sky the"
# Reversed String should not contain leading, trailing, or multiple whitespaces.
def reverseWords(s):
   n = len(s) - 1
   word = str = ''
   prevCh = ''

   for i in range(n,-1,-1):
       char = s[i]
       if char == ' ':
           if prevCh != '' and prevCh != ' ':
               if i!=0:
                   str += word + ' '
                   word = ''
               else:
                   str += word
       else:
           word = char + word
           if i == 0:
               str += word
       prevCh = char

   if str!='' and str[-1] == ' ':
        return str[:-1]
   else:
        return str

print(reverseWords('the sky is blue'))
print(reverseWords(' Hi There '))
print(reverseWords('     Hi      There   '))
print(reverseWords('H'))
print(reverseWords(''))
print(reverseWords('  '))
print(reverseWords('Y  '))
