# Print and reverse print a character array
import re
def simplePrint(str,lenStr,end):
    # Base
    if end == 0:
        return

    # Assumption + Induction Step
    simplePrint(str,lenStr,end-1)
    if end == lenStr-1:
        print(str[end-1])
    else:
        print(str[end-1],end='')

def reversePrint(str,end):
    # Base
    if end == 0:
        print('')
        return True
    # Assumption + Induction Step
    print(str[end-1],end='')
    reversePrint(str,end-1)

str = 'abcd'
simplePrint(str,4,4)
str = 'abcdefg'
reversePrint(str,7)
str = 'raceacar'
reversePrint(str,8)
