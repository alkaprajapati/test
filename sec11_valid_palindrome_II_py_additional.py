# Given a non-empty string s, you may delete at most one character. Judge whether you can make it a palindrome
import re
def validPalindromeRecur(s,start,end):
    # This is 68.63
    if s[start] != start[end]:
        return False
    # This approach is python specific
    s = re.sub('[^a-zA-Z0-9]','',s)
    s = list(s.lower())

    if s == s[::-1]:
        return True
 
    start = 0
    end = len(s) - 1
    while start <= end:
        if s[start] != s[end]:
            subStr = s[start+1:end+1]
            if subStr != subStr[::-1]:
                subStr = s[start:end]
                if subStr != subStr[::-1]:
                    return False
                else:
                    return True
            else:
                return True
        start += 1
        end -= 1
    return True

print(validPalindromeII("abaca"))
print(validPalindromeII("aadeaa"))
print(validPalindromeII("aaeadcaaaedaeaa"))
print(validPalindromeII("deeee"))
