def rotateImage(matrix):
    # 5% better
    # print(matrix)
    cols=rows=len(matrix)

    # Find the transpose
    for i in range(rows):
        for j in range(i,cols):
            (matrix[i][j],matrix[j][i]) = (matrix[j][i],matrix[i][j])
    # print("transpose:",matrix)

    # Now reverse each row
    # for row in range(rows):
    #     start, end  = 0, cols-1
    #     while start < end:
    #         (matrix[row][start],matrix[row][end]) = (matrix[row][end],matrix[row][start])
    #         start+=1
    #         end-=1
    for row in range(rows):
        matrix[row] = matrix[row][::-1]

    return matrix

matrix = [[1,2,3],[4,5,6],[7,8,9]]
print(rotateImage(matrix))
# matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]]
# print(rotateImage(matrix))
# matrix = []
# print(rotateImage(matrix))
