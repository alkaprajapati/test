class MinPriorityQueue:

    def __init__(self):
        self.pq =[]
        self.pqLen = 0
        pass

    def getParent(self,childIndex):
        parentIndex = (childIndex-1) // 2
        if parentIndex >= 0:
            return parentIndex
        return None

    def getMinChild(self,parentIdx):
        leftChildIdx = ( (2 * parentIdx) + 1)
        rightChildIdx = leftChildIdx+1
        if leftChildIdx >= self.pqLen and  rightChildIdx >= self.pqLen:
            return None
        elif leftChildIdx >= self.pqLen:
            return rightChildIdx
        elif rightChildIdx >= self.pqLen:
            return leftChildIdx
        else:
            if self.pq[leftChildIdx] > self.pq[rightChildIdx]:
                return rightChildIdx
            else:
                return leftChildIdx

    def upHeapify(self,childIdx,pq):
        while childIdx >= 0:
            parentIdx = self.getParent(childIdx)
            if parentIdx == None: return
            if pq[childIdx] < pq[parentIdx]:
                (pq[parentIdx],pq[childIdx]) = (pq[childIdx],pq[parentIdx])
                childIdx    = parentIdx
            else:
                return

    def downHeapify(self,parentIdx,pq):
        while parentIdx < self.pqLen:
            childIdx = self.getMinChild(parentIdx)
            if childIdx == None: return

            if pq[parentIdx] > pq[childIdx]:
                (pq[parentIdx],pq[childIdx]) = (pq[childIdx],pq[parentIdx])
                parentIdx    = childIdx
            else:
                return

    def insert(self,elem):
        self.pq.append(elem)
        self.childIdx = self.pqLen
        self.pqLen += 1
        self.upHeapify(self.childIdx,self.pq)

    def getMin(self):
        if not self.isEmpty():
            return self.pq[0]
        return  None

    def isEmpty(self):
        if  self.pqLen == 0:
            return True
        return False

    def getSize(self):
        return self.pqLen

    def removeMin(self):
        (self.pq[0],self.pq[self.pqLen-1]) = (self.pq[self.pqLen-1],self.pq[0])
        self.pq.pop()
        self.pqLen-=1
        self.downHeapify(0,self.pq)
# arr= [12,6,5,100,1,9,0,14]
minHeap = MinPriorityQueue()
minHeap.insert(12)
minHeap.insert(6)
print(minHeap.pq)
minHeap.insert(5)
print(minHeap.pq)
minHeap.insert(100)
print(minHeap.pq)
minHeap.insert(1)
print(minHeap.pq)
minHeap.insert(9)
print(minHeap.pq)
minHeap.insert(0)
print(minHeap.pq)
minHeap.insert(14)
print(minHeap.getSize())
print(minHeap.getMin())
print(minHeap.isEmpty())
minHeap.removeMin()
print(minHeap.pq)
minHeap.removeMin()
print(minHeap.pq)
minHeap = MinPriorityQueue()
minHeap.insert(10)
minHeap.insert(20)
minHeap.insert(100)
minHeap.insert(30)
minHeap.insert(40)
print(minHeap.pq)
minHeap.removeMin()
print(minHeap.pq)
