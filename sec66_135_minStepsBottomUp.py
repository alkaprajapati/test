def minStepsBottomUp(n):
    arr=[float('inf')]*(n+1)

    arr[0] = arr[1] = 0

    for i in range(2,n+1):
        # decrement by 1
        opt1 = arr[i-1]
        # Divide by 2
        opt2 = float('inf')
        if i % 2 == 0:
            opt2 = arr[i//2]
        # Divide by 3
        opt3 = float('inf')
        if i % 3 == 0:
            opt3 = arr[i//3]
        # print(opt3,opt2,opt1)
        # print(arr)
        arr[i] = min(opt1,opt2,opt3)+1
    return arr[n]

# print(minStepsBottomUp(2))
# print(minStepsBottomUp(3))
print(minStepsBottomUp(7))
print(minStepsBottomUp(11))
print(minStepsBottomUp(265))
