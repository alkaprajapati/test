import sec64_110_maxPriorityQ_additional as maxhp

def kSortedArr(arr,k):

    maxPQ = maxhp.MaxPriorityQueue()
    # Push first K elements in to max heap Q
    for i in range(k):
        # k log k
        maxPQ.insert(arr[i])

    # Initialize pointers
    start = 0
    i = k

    # this will set n - k elements
    while i<len(arr):
        # n - k times
        # remove top of max heap Q and insert at start
        # log k
        arr[start] = maxPQ.removeMax()
        start += 1
        # insert elel=m at i into max HQ
        # log K
        maxPQ.insert(arr[i])
        i+=1

    # For elements left (see test case two)
    while not maxPQ.isEmpty():
        # remove top of max heap Q and insert at start
        # Constant * log k
        arr[start] = maxPQ.removeMax()
        start += 1

arr = [10,15,6,4,5]
kSortedArr(arr,2)
print(arr)

arr = [10,12,6,7,9]
kSortedArr(arr,3)
print(arr)
