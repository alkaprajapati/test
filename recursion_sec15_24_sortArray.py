# Check if array is sorted or not using recursion
# Check if last elem > prev elem
# This happens till arr is reduced to len 1||0 (which is our base case)
def check_sorted_array(arr,arrLen):

    if arrLen == 1 or arrLen == 0:
        return True

    if  arr[arrLen-1] > arr[arrLen-2]:
        isPrevArrSorted = check_sorted_array(arr[:arrLen-1],arrLen-1)
        return isPrevArrSorted
    else:
        return False

print(check_sorted_array([1,2,3,4],4))
print(check_sorted_array([1,2,4,3],4))
print(check_sorted_array([3],1))
print(check_sorted_array([],0))
print(check_sorted_array([2,8],2))
print(check_sorted_array([8,2],2))
