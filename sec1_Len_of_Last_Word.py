def lengthOfLastWord(s) :
    sLen = len(s)
    i = sLen - 1
    count = 0
    word_start = False

    while (i >= 0) :
        word = s[i]
        if(word != " "):
            word_start = True
            count+=1
        else:
            if word_start == True:
                break
        i-=1
    return count

print( lengthOfLastWord ( "Hello word" ) )
print( lengthOfLastWord ( "Hello word     " ) )
print( lengthOfLastWord ( "Hello wo rd " ) )