# Given a linked list, remove the n-th node from the end of list and return its head.
# Example:
# Given linked list: 1->2->3->4->5, and n = 2.
# After removing the second node from the end, the linked list becomes 1->2->3->5.
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        slow = head
        fast = head

        for i in range(n):
            fast = fast.next
            if fast == None:
                # This implies we trying to access N, where N > len of list
                # In this case adjust N = len of list
                break

        if fast == None:
            return slow.next

        while fast.next != None:
            slow = slow.next
            fast = fast.next

        slow.next = slow.next.next
        return head
