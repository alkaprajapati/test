s = "hello world"
# Just FYI in slice [::-1] means start at the end of the string and end at position 0, move with the step -1
# you can play with the step to get alternate letters if needed
# for python slicing is O(n)
s = s[::-1]
print("here",s)

#or simply use the reverse function T(n)
s = list("dumb dumb")
s.reverse()
print( s )
