# Print all the position of the Element in a given List; Print nothing if not found
def allPosOfElem(arr,arrLen,elem):
    # End -> Start
    # Base
    if arrLen == 0:
        return

    allPosOfElem(arr,arrLen-1,elem)

    # Assumption + Induction Step
    if elem == arr[arrLen-1]:
        print(arrLen-1)

print("Case1:")
allPosOfElem([1,2,3,4,5],5,4)
print("Case2:")
allPosOfElem([1,4,3,4,5],5,4)
print("Case3:")
allPosOfElem([1,0,3,0,4],5,0)
print("Case4:")
allPosOfElem([1,2,5,5,5],5,9)
