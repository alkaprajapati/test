#--------------1. List--------------------
listDemo = []
listDemo2 = list()
listDemo3 = listDemo
listDemo.append('a')
listDemo.append('d')
listDemo.insert(1,'c')
listDemo.insert(1,'b')
listDemo.append('e')
listDemo.append('e')
listDemo.append('f')
listDemo.append('g')
listDemo.append('f')
print(listDemo)
indexReturned = listDemo.index('e')
print("Index of obj 'e': ",indexReturned)
eCount = listDemo.count('e')
print("Count of obj e: ",eCount)
# remove(value) -> removes only first matching value
listDemo.remove('e')
print("List after removing e:", listDemo)
print("ListDemo3 is not a copy, it points to the original list:", listDemo3)
# following To make a pure copy
listDemo4 = listDemo[:]
listDemo5 = list(listDemo)
listDemo6 = listDemo.copy()
#diff btw del and pop, they both remove the item at specified index but pop also returns the object it deleted
del listDemo[0]
print("List after deleting index 0:", listDemo)
itemPop = listDemo.pop()
print("List after pop with no index will remove last item:", listDemo)
itemPop = listDemo.pop(3)
print("List after pop(3):", listDemo)
print("Obj returned by pop: ",itemPop)
listDemo.append('a')
listDemo.append('ac')
listDemo.append('ab')
listDemo.append('aca')
print("Before sort:",listDemo)
listDemo.sort()
print("After sort:",listDemo)
#Slicing
print("slicing l[ indexfrom : indexto+1 ]",listDemo[1:3])
print("Reversing indexing l[-o:-n]",listDemo[-3:-1])
print("Reversing the order l[::-step]",listDemo[::-1])
#Concatenating
listDemo2.append(1)
listDemo = listDemo + listDemo2
print("After concatenation:",listDemo)
#---------------2. Dictionary --------------
# {key: value}
dictDemo = dict.fromkeys( ('a','b','c'), (1,2,3) )
dictDemo = dict.fromkeys( ('a','b','c'), 1 )
dictDemo1 = {}
dictDemo2 = {'abc':1, 'efg':2}
for key,val in dictDemo.items():
    print("key,value:",key,val)
for key in dictDemo.keys():
    print("key,value:",key,dictDemo[key])
for val in dictDemo.values():
    print("value:",val)
print( dictDemo.get('a') )
dictDemo1 = dictDemo.copy()
dictDemo.pop('b')
print("Dictionary after pop:",dictDemo)
dictDemo.popitem()
print("Dictionary after popitem:",dictDemo)
print("Dictionary after clear:",dictDemo)
dictDemo.clear()
# The setdefault() method returns the value of the item with the specified key.
# If the key does not exist, insert the key, with the specified value, see example below
x = dictDemo.setdefault('d','3')
print(x)
x = dictDemo.setdefault('a','3')
print(x)
