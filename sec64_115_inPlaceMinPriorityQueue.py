class MinPriorityQueue:

    def __init__(self,list):
        self.pq = list
        self.pqLen = 0
        # self.pqLen = len(arr)
        pass

    def getParent(self,childIndex):
        parentIndex = (childIndex-1) // 2
        if parentIndex >= 0:
            return parentIndex
        return None

    def getMinChild(self,parentIdx):
        leftChildIdx = ( (2 * parentIdx) + 1)
        rightChildIdx = leftChildIdx+1
        if leftChildIdx >= self.pqLen and  rightChildIdx >= self.pqLen:
            return None
        elif leftChildIdx >= self.pqLen:
            return rightChildIdx
        elif rightChildIdx >= self.pqLen:
            return leftChildIdx
        else:
            if self.pq[leftChildIdx] > self.pq[rightChildIdx]:
                return rightChildIdx
            else:
                return leftChildIdx

    def upHeapify(self,childIdx,pq):
        while childIdx >= 0:
            parentIdx = self.getParent(childIdx)
            if parentIdx == None: return
            if pq[childIdx] < pq[parentIdx]:
                (pq[parentIdx],pq[childIdx]) = (pq[childIdx],pq[parentIdx])
                childIdx    = parentIdx
            else:
                return

    def downHeapify(self,parentIdx,pq):
        while parentIdx < self.pqLen:
            childIdx = self.getMinChild(parentIdx)
            if childIdx == None: return

            if pq[parentIdx] > pq[childIdx]:
                (pq[parentIdx],pq[childIdx]) = (pq[childIdx],pq[parentIdx])
                parentIdx    = childIdx
            else:
                return

    def buildHeap(self):
        i  = -1
        for elem in self.pq:
            i+=1
            self.pqLen += 1
            self.upHeapify(i,self.pq)

    def getMin(self):
        if not self.isEmpty():
            return self.pq[0]
        return  None

    def isEmpty(self):
        if  self.pqLen == 0:
            return True
        return False

    def getSize(self):
        return self.pqLen

    def removeMinNTimes(self,n):
        for i in range(n):
            self.removeMin()
    # @property
    # def pq(self):
    #     return self.pq[:self.pqLen]

    def removeMin(self):
        (self.pq[0],self.pq[self.pqLen-1]) = (self.pq[self.pqLen-1],self.pq[0])
        # self.pq.pop()
        self.pqLen-=1
        self.downHeapify(0,self.pq)

arr= [5,8,3,1,6,0]
minHeap = MinPriorityQueue(arr)
minHeap.buildHeap()
print(arr)
print(minHeap.pqLen)
minHeap.removeMin()
print(arr)
print(minHeap.pqLen)
print(minHeap.pq)
minHeap.removeMinNTimes(3)
print(minHeap.pqLen)
print(arr[:minHeap.pqLen])
