# Given a complete binary tree, count the number of nodes.
# Note: In a complete binary tree every level, except possibly the last, is completely filled, and all nodes in the last level are as far left as possible. It can have between 1 and 2h nodes inclusive at the last level h.
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def countNodes(self, root: TreeNode) -> int:
        stack = []
        curr = root
        count = 0

        while curr != None or stack!=[]:

            while curr!= None:
                stack.append(curr)
                curr = curr.left

            count+=1
            curr = stack.pop()
            curr = curr.right
        return count


