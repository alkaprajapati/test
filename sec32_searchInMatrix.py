# Write an efficient algorithm that searches for a value in an m x n matrix. This matrix has the following properties:
# Integers in each row are sorted from left to right.
# The first integer of each row is greater than the last integer of the previous row.
# STAIRCASE APPROACH
def searchMatrix(matrix,target):
    endRow = len(matrix) - 1
    if endRow < 0: return False
    col = endCol = len(matrix[0]) - 1
    if  endCol < 0: return False
    row = 0

    if  ( target < matrix[0][0] or target > matrix[endRow][endCol]): return False

    while row<=endRow and col>=0:
        startElem = matrix[row][col]
        # print(row,col,startElem)
        if  target == startElem:
            return True
        elif target > startElem:
            row +=1
        else:
            col-=1
    return False
matrix=[[]]
print( searchMatrix(matrix,1) )
matrix=[[1]]
print( searchMatrix(matrix,5) )
matrix = [  [1,   3,  5,  7],  [10, 11, 16, 20],  [23, 30, 34, 50] ]
print( searchMatrix(matrix,0) )
print( searchMatrix(matrix,3) )
print( searchMatrix(matrix,10) )
print( searchMatrix(matrix,16) )
print( searchMatrix(matrix,20) )
print( searchMatrix(matrix,23) )
print( searchMatrix(matrix,30) )
print( searchMatrix(matrix,50) )
print( searchMatrix(matrix,51) )
print( searchMatrix(matrix,9) )
matrix = [  [1,   3,  5,  7],  [10, 11, 16, 20],  [23, 30, 34, 50] ]
target = 13
print( searchMatrix(matrix,target) )
