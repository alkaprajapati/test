# Print all Subsequent strings for a given string
# Include and exclude every character once from the current string
def subsequenceStrings(str,out):
    if len(str) == 0:
        print(out)
        return

    # exclude the character from output string
    subsequenceStrings(str[1:],out)
    # include the character from output string
    subsequenceStrings(str[1:],out+str[0])

output = ''
subsequenceStrings('abc',output)
output = []
