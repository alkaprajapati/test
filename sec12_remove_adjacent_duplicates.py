# Given a string S of lowercase letters, a duplicate removal consists of choosing two adjacent and equal letters, and removing them.
# We repeatedly make duplicate removals on S until we no longer can.
# Return the final string after all such duplicate removals have been made.  It is guaranteed the answer is unique.
def remAdjacentDuplicates(S):
    # TODO: Needs improvement
    # 57.3% T(n) O(n)
    stack = ['']
    for ch in S:
        if stack[-1] != ch:
            stack.append(ch)
        else:
            stack.pop()
    print( ''.join(stack))
    # For coding lag without join
    # s = s + stack.pop() while stack is not empty
    return ''.join(stack)


remAdjacentDuplicates('saasa')
# remAdjacentDuplicates('')
remAdjacentDuplicates('aaaaaaaab')
remAdjacentDuplicates('aacaaaaaab')
