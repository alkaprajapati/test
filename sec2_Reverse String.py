def reverseString(s):
    """
    We are using pointer approach in which we start from first and last index
    Swap their content
    move ahead with first++ last--
    Doing till mid of the string
    This code was 80% more performance efficient
    T(n) S(1)
    """
    sLen = len(s) - 1

    if sLen <= 0:
        return

    mid = sLen // 2
    # In languages with no reverse indexing as in python, just use j=slen-1
    i, j = 0, -1
    while(i <= mid):
        x = s[j]
        s[j] = s[i]
        s[i] = x
        i += 1
        j-=1
# the below step is necessary; remember in Python for a string we cannot do s[index1] = s[index2]. Change it to list first
s = list("hello")
reverseString(s)
print(s)
s = list("cutopia")
reverseString(s)
print(s)
