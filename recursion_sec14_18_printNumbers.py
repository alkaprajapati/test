# print till 1.....n
def printNAsc(n):
    # Base
    if  n == 0:
        return
    # Assumption + Induction Step
    printNAsc(n-1)
    print(n)

# and print n n-1.... 2 1
def printNDesc(n):
    # Base
    if n == 0:
        return
    # Assumption + Induction Step
    print(n)
    printNDesc(n-1)

print("Ascending Order")
printNAsc(4)
print("Descending Order")
printNDesc(4)
