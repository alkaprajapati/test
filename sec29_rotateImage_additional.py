def rotateImage(matrix):
    cols=rows = len(matrix)
    arr = [[0 for i in range(cols)] for j in range(rows)]

    for row in range(0,rows):
        newCol = rows-1-row
        newRow = 0
        for col in range(0,rows):
             arr[newRow][newCol] = matrix[row][col]
             newRow+=1
    return arr

matrix = [[1,2,3],[4,5,6],[7,8,9]]
print(rotateImage(matrix))
