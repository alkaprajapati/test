class HashTable:
    def __init__(self,max):
        self.max = max
        self.arr = [[] for i in range(self.max)]

    def get_hash1(self, key):
        hash = 0
        for char in key:
            hash += ord(char)
        return hash % self.max

    def get_hash(self, key):
        hash = 0
        p = 37
        base = 1
        for char in key:
            hash += ord(char) * base
            base = ( base * 37 ) % self.max
        # print(hash % self.max)
        return hash % self.max

    def __getitem__(self, key):
        arr_index = self.get_hash(key)
        for keyValue in self.arr[arr_index]:
            if keyValue[0] == key:
                return keyValue[1]

    def __setitem__(self, key, val):
        h = self.get_hash(key)
        found = False
        # we are storing a linked list at every index to handle collision in case of a hash function value collision
        for index, element in enumerate(self.arr[h]):
            if len(element)==2 and element[0] == key:
                self.arr[h][index] = (key,val)
                found = True
        if not found:
            self.arr[h].append((key,val))

    def __delitem__(self, key):
        arr_index = self.get_hash(key)
        for index, keyValue in enumerate(self.arr[arr_index]):
            # print(keyValue)
            if keyValue[0] == key:
                # print("del",index)
                print(keyValue)
                del self.arr[arr_index][index]

hashTableIns = HashTable(50)
hashTableIns["ke2"] = 8
hashTableIns["key1"] = 28
hashTableIns["key2"] = 29
hashTableIns["key3"] = 30
hashTableIns["k4ey"] = 311
hashTableIns["key4"] = 31
hashTableIns["kye4"] = 31
hashTableIns["ke4y"] = 39
hashTableIns["4key"] = 39
print(hashTableIns.arr)
hashTableIns["key4"] = 35
print(hashTableIns.arr)
print(hashTableIns["key4"])
print(hashTableIns["4key"])
del hashTableIns["4key"]
print(hashTableIns.arr)
