# The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
# (you may want to display this pattern in a fixed font for better legibility)
# P   A   H   N
# A P L S I I G
# Y   I   R
# And then read line by line: "PAHNAPLSIIGYIR"
# Write the code that will take a string and make this conversion given a number of rows:
# string convert(string s, int numRows);
def zigZagConversion(s,numRows):

    if  numRows == 1 or len(s) <= numRows:
        return s

    e = numRows - 1
    index, col, lenS = 0, 0, len(s) - 1
    arr = ['']*numRows
    flag = True

    while (flag):
        if col % e != 0:
            start, end, direction = e-1, 0, -1
        else:
            start, end, direction = 0, numRows, 1
            col +=1

        for row in range(start,end,direction):
            arr[row] += s[index]
            index +=1
            if  direction == -1:
                col += 1
            if index > lenS:
                flag = False
                break

    return ''.join(arr)
print(zigZagConversion('PAYPALISHIRING',1))
print(zigZagConversion('PAY',3))
print(zigZagConversion('PA',3))
print(zigZagConversion('',3))
# PAHNAPLSIIGYIR
print(zigZagConversion('PAYPALISHIRING',3))
# PINALSIGYAHRPI
print(zigZagConversion('PAYPALISHIRING',4))

