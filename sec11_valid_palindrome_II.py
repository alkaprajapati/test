# Given a non-empty string s, you may delete at most one character. Judge whether you can make it a palindrome
# I do not believe it to be T(n) as mentioned the video but T(n2) and S(1)
import re
def check(s,start,end):
    while start <= end:
        if s[start] != s[end]:
            return False
        start+=1
        end-=1
    return True

def validPalindromeII(s):
    s = re.sub('[^a-zA-Z0-9]','',s)
    s = list(s.lower())
    start = 0
    end = len(s) - 1

    while start <= end:
            if  s[start] !=s[end]:
                if check(s,start+1,end) == False:
                    return check(s,start,end-1)
                else:
                    return True
            start += 1
            end -= 1
    return True

# print(validPalindromeII("abaa"))
print(validPalindromeII("abaca"))
print(validPalindromeII("aadeaa"))
print(validPalindromeII("aadabaeaa"))
