# Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.
# (i.e.,  [0,1,2,4,5,6,7] might become  [4,5,6,7,0,1,2]).
# Find the minimum element.
# You may assume no duplicate exists in the array.
def minRotatedArr(nums):
    low = 0
    lenN = len(nums)
    high = lenN -1

    if nums[low] <= nums[high]:
         return nums[low]

    while low <= high:
        mid = ( high + low) // 2
        midElem =  nums[mid]
        next = ( mid + 1 ) % lenN
        prev = ( mid + lenN - 1) % lenN

        if midElem <= nums[prev] and midElem <= nums[next]:
            return nums[mid]
        elif midElem <= nums[high]:
            high = mid - 1
        elif midElem >= nums[low]:
            low = mid + 1
nums = [3,4,5,0,1,2]
print(minRotatedArr(nums))
nums = [4,5,0,1,2,3]
print(minRotatedArr(nums))
nums = [5,0,1,2,3,4]
print(minRotatedArr(nums))
nums = [1,2,3,4,5,0]
print(minRotatedArr(nums))
nums = [4,5,6,7,0,1,2]
print(minRotatedArr(nums))
