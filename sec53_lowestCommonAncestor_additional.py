# Given a binary search tree (BST), find the lowest common ancestor (LCA) of two given nodes in the BST.
# According to the definition of LCA on Wikipedia: “The lowest common ancestor is defined between two nodes p and q as the lowest node in T that has both p and q as descendants (where we allow a node to be a descendant of itself).”
# Given binary search tree:  root = [6,2,8,0,4,7,9,null,null,3,5]
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
class Solution:
    def lowestCommonAncestor(self, root: 'TreeNode', p: 'TreeNode', q: 'TreeNode') -> 'TreeNode':
        if not root:
            return None

        p_val = p.val
        q_val = q.val
        curr_val = root.val

        if curr_val > p_val and curr_val > q_val:
            return self.lowestCommonAncestor(root.left,p,q)
        elif curr_val < p_val and curr_val < q_val:
            return self.lowestCommonAncestor(root.right,p,q)
        else:
            return root
