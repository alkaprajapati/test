# Given a binary tree, return the inorder traversal of its nodes' values.
# Example:
# Input: [1,null,2,3]
#    1
#     \
#      2
#     /
#    3
# Output: [1,3,2]
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    # Using Recursion
    def inorderTraversal(self, root: TreeNode) -> List[int]:
        # print(root)
        ans =[]
        self.inorderTraversal1(root,ans)
        return ans

    def inorderTraversal1(self, root: TreeNode, ans: List) -> List[int]:

        if root == None:
            return

        self.inorderTraversal1(root.left,ans)
        ans.append(root.val)
        self.inorderTraversal1(root.right,ans)
