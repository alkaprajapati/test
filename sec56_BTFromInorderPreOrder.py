# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def buildTree(self, preorder: List[int], inorder: List[int]) -> TreeNode:
        nodes = len(inorder) - 1
        return self.buildTreehelper(inorder,preorder,0,nodes,0,nodes)

    def buildTreehelper(self, ino, pre, inS, inE, preS, preE):

        if inS > inE: return None
        rootVal   = pre[preS]

        print(rootVal,preS,inS,inE)
        rootIdx = None

        # Root Index
        for i in range(inS,inE+1):
            if ino[i] == rootVal:
                rootIdx = i
                break

        leftinS   = inS
        leftinE   = rootIdx -1
        leftpreS  = preS+1
        leftpreE  = leftpreS + leftinE - leftinS
        rightinS  = rootIdx + 1
        rightinE  = inE
        rightpreS = leftpreE + 1
        rightpreE = preE

        root = TreeNode(rootVal)
        root.left  = self.buildTreehelper(ino,pre,leftinS,leftinE,leftpreS,leftpreE)
        root.right = self.buildTreehelper(ino,pre,rightinS,rightinE,rightpreS,rightpreE)
        return root
