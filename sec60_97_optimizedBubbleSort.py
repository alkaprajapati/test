def bubbleSort(arr):

    lenArr = len(arr)

    for i in range(1,lenArr):
        flag = 0
        for j in range(0,lenArr-1):
            if arr[j]>arr[j+1]:
                flag = 1
                (arr[j],arr[j+1]) = (arr[j+1],arr[j])
        if flag == 0:
            break

    return arr

print(bubbleSort([1,4,2,4,6,8,7,1]))
print(bubbleSort([7,5,3,6,8]))
print(bubbleSort([7]))
print(bubbleSort([7,1]))
