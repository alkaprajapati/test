# Given a binary tree, determine if it is a valid binary search tree (BST).
# Assume a BST is defined as follows:
# The left subtree of a node contains only nodes with keys less than the node's key.
# The right subtree of a node contains only nodes with keys greater than the node's key.
# Both the left and right subtrees must also be binary search trees.
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
class Solution:
    def isValidBST(self, root: TreeNode) -> bool:
        prev  = -float('inf')
        stack = []
        curr  = root

        while curr != None or stack != []:
            while curr != None:
                stack.append(curr)
                curr = curr.left

            curr = stack.pop()
            if curr.val <= prev:
                return False
            prev = curr.val
            curr = curr.right
        return True

# using O(n)
class SolutionN:
    def isValidBST(self, root: TreeNode) -> bool:
        ans =[]
        self.inorderTraversal1(root,ans)
        prev = -float('inf')
        for i in ans:
            if i <= prev:
                return False
            prev = i
        return True

    def inorderTraversal1(self, root: TreeNode, ans: List) -> List[int]:

        if root == None:
            return

        self.inorderTraversal1(root.left,ans)
        ans.append(root.val)
        self.inorderTraversal1(root.right,ans)
