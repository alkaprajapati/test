# Given a binary tree, find its maximum depth.
# The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
# Note: A leaf is a node with no children.
# Example:
# Given binary tree [3,9,20,null,null,15,7],
#     3
#    / \
#   9  20
#     /  \
#    15   7
# return its depth = 3.
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:

    def minDepth(self, root: TreeNode) -> int:

        if root == None:
            return 0

        queue = []
        depth = 1
        queue.append([root,depth])

        while queue != []:
            frontItem = queue.pop(0)
            front     = frontItem[0]
            depth     = frontItem[1]

            if not (front.left) and not(front.right):
                return depth

            depth += 1
            if front.left:
                queue.append([front.left,depth])
            if front.right:
                queue.append([front.right,depth])
