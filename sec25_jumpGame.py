# Given an array of non-negative integers, you are initially positioned at the first index of the array.
# Each element in the array represents your maximum jump length at that position.
def jumpGame(nums):
    maxreach = 0
    for i in range(len(nums)):
        if i > maxreach:
            return False
        maxreach = max(maxreach,i+nums[i])
    return True

nums=[3,2,1,0,4]
print(jumpGame(nums))
nums=[3,3,1,0,4]
print(jumpGame(nums))
nums=[3,3,1,0,2,1,0,4]
print(jumpGame(nums))
nums=[3,3,1,1,0]
print(jumpGame(nums))
