def binarySearch(arr,key):
    # If array is really big, and there a possibility for integer overflow then consider mid calculation specified below
    start   = 0
    end     = len(arr)-1

    while start <= end:
        mid = start + ( ( end - start ) // 2 )
        if   arr[mid] == key:
            return mid
        elif arr[mid] < key:
            start = mid + 1
        elif arr[mid] > key:
            end = mid - 1
    return None

print(binarySearch([1,2,3,4,5,6,7,8],2))
print(binarySearch([5,6,7,8,9],8))
print(binarySearch([7],7))
print(binarySearch([1,7],9))
