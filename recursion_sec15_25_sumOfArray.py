# Find the sum of elements of an array using recursion
# last elem + prevSum or first elem + prevSum
# In last arroach we are using an index, instead of passing a copy of array for every recursion
# This happens till arr is reduced to 0 length (which is our base case)
def sumOfArray(arr,arrLen):
    # Base
    if arrLen == 0:
        return 0
    # Assumption + Induction Step
    prevSum = sumOfArray(arr[:arrLen-1],arrLen-1)

    return arr[arrLen - 1] + prevSum

def sumOfArray1(arr,arrLen):
    # Base
    if arrLen == 0:
        return 0
    # Assumption + Induction Step

    prevSum = sumOfArray1(arr[1:],arrLen-1)
    return arr[0] + prevSum

def sumOfArray2(arr,arrLen,startIndex):
    # Base
    if arrLen == startIndex:
        return 0
    # Assumption + Induction Step
    prevSum = sumOfArray2(arr,arrLen,startIndex+1)
    return arr[startIndex] + prevSum

print(sumOfArray([1,2,3,4,5],5))
print(sumOfArray1([1,2,3,4,5],5))
print(sumOfArray2([1,2,3,4,5],5,0))
print(sumOfArray([3],1))
print(sumOfArray1([3],1))
print(sumOfArray2([3],1,0))
print(sumOfArray([],0))
print(sumOfArray1([],0))
print(sumOfArray2([],0,0))
print(sumOfArray([8,2],2))
print(sumOfArray1([8,2],2))
print(sumOfArray2([8,2],2,0))
print(sumOfArray([8,2,1,2,-1],5))
print(sumOfArray1([8,2,1,2,-1],5))
print(sumOfArray2([8,2,1,2,-1],5,0))
