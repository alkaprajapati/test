def moveT(disk,fromT,destinationT):
    print('Move disk ',disk,' from ',fromT,' To ',destinationT)

def hanoiPrintSteps(n,fromT,destiantionT,helperT,disk=0):
    #Base
    if n == 1:
        moveT(disk,fromT,destiantionT)
        return
    # Assumption + Induction step
    #  Steps to transfer n-1 disks from Original to Helper tower
    #  1 step to transfer the nth disk from Original to Destination tower
    # Steps to transfer the n-1 disk from Helper to Destination tower
    hanoiPrintSteps(n-1,fromT,helperT,destiantionT,n-1)
    hanoiPrintSteps(1,fromT,destiantionT,helperT,n)
    hanoiPrintSteps(n-1,helperT,destiantionT,fromT,n-1)

hanoiPrintSteps(3,'FromTower','DestinationTower','HelperTower',0)

