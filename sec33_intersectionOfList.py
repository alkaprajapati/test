# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None
# O(n) S(1)
class Solution:
    def getLength(self,head: ListNode):
        count = 0
        while head!=None:
            count+=1
            head=head.next
        return count

    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        if not (headA and headB):
            return None

        lenA = self.getLength(headA)
        lenB = self.getLength(headB)

        startAt = abs(lenB - lenA)

        if lenA <= lenB:
            for i in range(0,startAt):
                headB = headB.next
        else:
            for i in range(0,startAt):
                headA = headA.next

        while headA!=None and headB!=None:
            if headA == headB:
                return headA
            headA = headA.next
            headB = headB.next

        return None
