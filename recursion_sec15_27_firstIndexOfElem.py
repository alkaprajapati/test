# Get the first Index at which the element was found; if not found return -1
# Traversing direction is onto you...I prefer using an index to track iteration for next recursion call
def firstIndexOfElem(arr,arrLen,elem,firstIndex):
    # Base
    if arrLen == firstIndex:
        return -1
    # Assumption + Induction Step
    if elem == arr[firstIndex]:
        return firstIndex
    else:
        return firstIndexOfElem(arr,arrLen,elem,firstIndex+1)

print(firstIndexOfElem([1,2,3,4,5],5,4,0))
print(firstIndexOfElem([1,4,3,4,5],5,4,0))
print(firstIndexOfElem([1,0,3,0,4],5,4,0))
print(firstIndexOfElem([1,2,3,4,5],5,9,0))
