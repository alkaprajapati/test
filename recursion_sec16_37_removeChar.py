# Remove a character from a string
# As String in python are immutable wr are not replacing char at any index.
# In one approach we can simply build a new string
# In another approach we send string as a list
def removeChar(str,char):
    # Base
    if str == '':
        return ''

    if str[0] == char:
        return removeChar(str[1:],char)
    else:
        return str[0] + removeChar(str[1:],char)

# If string converted to a list
def removeChar1(str,char):
    # Base
    if str == []:
        return

    if str[0] == char:
        str[0:] = str[1:]
        # equivanlent to for loop
        return removeChar1(str[:-1],char)
    else:
        return removeChar1(str[1:],char)

print(removeChar('','a'))
print(removeChar('abc','a'))
print(removeChar('bcdbaay','b'))

str = list(['a','b','c'])
removeChar1(str,'a')
print(str)
