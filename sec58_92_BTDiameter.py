    # Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def diameterOfBinaryTree(self, root: TreeNode) -> int:
        if root == None:
            return 0;
        
        ansLR = self.depth1(root.left) + self.depth1(root.right)
        ansL  = self.diameterOfBinaryTree(root.left)
        ansR  = self.diameterOfBinaryTree(root.right)
        return max(ansLR,ansL,ansR)
    
    def depth1(self, root):
        if root == None:
            return 0

        return max(self.depth1(root.left),self.depth1(root.right)) + 1
