# Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.
# Note: For the purpose of this problem, we define empty string as valid palindrome
import re
def validPalindrome(s):
    # 89.88% faster
    # T(n)
    # s(1)
    s = re.sub('[^a-zA-Z0-9]','',s)
    s = s.lower()
    start = 0
    end = len(s) - 1
    while start <= end:
        if s[start] != s[end]:
            return False
        start+=1
        end-=1
    return True

# Well in Python, it is a little more easy 99.88% faster
#     s = re.sub('[^a-zA-Z0-9]','',s).lower()
#     r = s[::-1]
#     return s == r
#     I do not want to say T(1) because I am not sure of taking s[start:stop:end] as one time unit
#   S(1)

# FYI some function isalnum() isalpha() isdigit() is
print(validPalindrome('race a car'))
print(validPalindrome('race e car'))
print(validPalindrome('A man, a plan, a canal: Panama'))
print(validPalindrome('0P'))
