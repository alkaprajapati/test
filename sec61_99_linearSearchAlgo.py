def linearSearch(arr,key):

    for i in range(len(arr)):
        if arr[i] == key:
            return i
    return None

print(linearSearch([1,4,2,4,6,8,7,1],1))
print(linearSearch([5,6,7,8,9],8))
print(linearSearch([7],7))
print(linearSearch([1,7],9))
