# Count Number of Zeros in a number
def countZeros(n):
    # Base
    if  n == 0:
        return 0
    # Assumption + Induction Step
    prevCount = countZeros(n//10)
    if  n % 10 == 0:
        return 1 + prevCount
    else:
        return prevCount

print(countZeros(0))
print(countZeros(9))
print(countZeros(91203))
print(countZeros(91200))
print(countZeros(901020))

