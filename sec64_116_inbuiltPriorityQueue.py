import heapq

# Min heap
arr= [5,8,3,1,6,0]
heapq.heapify(arr)
print(arr)
heapq.heappush(arr,2)
print(arr)
popElem = heapq.heappop(arr)
print(popElem)
print(arr)
# insers then pop
popElem = heapq.heappushpop(arr,-1)
print(arr,popElem)
# pop then insert
popElem = heapq.heapreplace(arr,-1)
print(arr,popElem)

# Max heap
arr= [5,8,3,1,6,0]
heap = []
heapq.heapify(heap)
for i in arr:
    heapq.heappush(heap,-1 * i)
for i in heap:
    print(-1*i)

