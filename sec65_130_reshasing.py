class HashTable:

    def __init__(self,max):
        self.max = max
        self.arr = [[] for i in range(self.max)]
        self.count = 0

    def get_hash1(self, key):
        hash = 0
        for char in key:
            hash += ord(char)
        return hash % self.max

    def get_hash(self, key):
        hash = 0
        p = 37
        base = 1
        for char in key:
            hash += ord(char) * base
            base = ( base * 37 ) % self.max
        # print(hash % self.max)
        return hash % self.max

    def __getitem__(self, key):

        arr_index = self.get_hash(key)

        for keyValue in self.arr[arr_index]:
            if keyValue[0] == key:
                return keyValue[1]

        load_factor = self.max

    def __setitem__(self, key, val):
        h = self.get_hash(key)
        found = False
        # we are storing a linked list at every index to handle collision in case of a hash function value collision
        for index, element in enumerate(self.arr[h]):
            if len(element)==2 and element[0] == key:
                self.arr[h][index] = (key,val)
                found = True
                self.count+=1

        if not found:
            self.arr[h].append((key,val))
            self.count+=1

        load_factor = self.count/self.max
        if load_factor > 0.7:
            print("rehasing required",load_factor)
            print("prebious arr",self.arr)
            self.rehash()

    def rehash(self):
        self.count = 0
        temp = self.arr[:]
        prevMax = self.max -1
        self.max *= 2
        self.arr = [[] for i in range(self.max)]
        found =  False
        for keyValue in temp:
            if keyValue != []:
                for keyValue in keyValue:
                    found = True
                    print(keyValue)
                    self.__setitem__(keyValue[0],keyValue[1])
                if found == False:
                    print(keyValue)
                    self.__setitem__(keyValue[0],keyValue[1])
        print(self.count)

        #python has garbage collection so we might as well delete the array directly
        for i in range(prevMax,-1,-1):
            # so that the linked list are also deleted
            # print("i",i,temp)
            del temp[i]
        del temp
        # print(temp)

    def __delitem__(self, key):
        arr_index = self.get_hash(key)
        for index, keyValue in enumerate(self.arr[arr_index]):
            # print(keyValue)
            if keyValue[0] == key:
                # print("del",index)
                # print(keyValue)
                del self.arr[arr_index][index]
                self.count-=1
    def getsize(self):
        return self.count

hashTableIns = HashTable(4)
hashTableIns["key1"] = 8
hashTableIns["key2"] = 28
hashTableIns["key3"] = 29
hashTableIns["key4"] = 30
hashTableIns["key5"] = 311
hashTableIns["key6"] = 31
hashTableIns["key7"] = 31
hashTableIns["key8"] = 39
hashTableIns["key9"] = 39
print(hashTableIns.arr)
del hashTableIns["key7"]
print(hashTableIns.arr)
print(hashTableIns.getsize())
