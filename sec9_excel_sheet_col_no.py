# Given a column title as appear in an Excel sheet, return its corresponding column number.
#
# For example:
#
#     A -> 1
#     B -> 2
#     C -> 3
#     ...
#     Z -> 26
#     AA -> 27
#     AB -> 28
#     ...
# S(1)
# T(n)
def titleToNumber(s):
    # In other lang for(i = len -1;i >= 0; I--)
    sum = 0
    # for i in range(-1,-len(s) -1,-1):
    #     pow = 26 ** (-i - 1)
    #     sum = sum + ( pow * ( ord(s[i]) - 64) )

    len1 = len(s)
    for ch in s:
        len1 -= 1
        sum += ( 26 ** len1) * (ord(ch) - 64)
    print(sum)

titleToNumber('AB')
titleToNumber('ZY')
titleToNumber('AAA')
# 26*26*1 + 26*1+1
