# Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right, level by level).
# For example:
# Given binary tree [3,9,20,null,null,15,7],
#     3
#    / \
#   9  20
#     /  \
#    15   7
# return its level order traversal as:
# [
#   [3],
#   [9,20],
#   [15,7]
# ]
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def levelOrder(self, root: TreeNode) -> List[List[int]]:

        if root == None:
            return []

        ans   = []
        queue = []
        queue.append(root)

        # True => Left to right
        direction = True

        while queue != []:
            lenQ = len(queue)
            level = [0]*lenQ


            for i in range(0,lenQ):
                front = queue.pop(0)
                if direction:
                    idx = i
                else:
                    idx = lenQ - 1 -i

                level[idx] = front.val

                if front.left:
                    queue.append(front.left)
                if front.right:
                    queue.append(front.right)
            direction = not direction
            ans.append(level)

        return ans
