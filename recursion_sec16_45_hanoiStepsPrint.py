def hanoiPrintSteps(n,fromT,destiantionT,helperT,disk=0):
    #Base
    if n == 0:
        return
    # Assumption + Induction step
    # Transfer n-1 disks from Original to Helper tower
    # Transfer the nth disk from Original to Destination tower
    # Transfer the n-1 disk from Helper to Destination tower
    hanoiPrintSteps(n-1,fromT,helperT,destiantionT,n-1)
    print('Move disk ',n,' from ',fromT,' To ',destiantionT)
    hanoiPrintSteps(n-1,helperT,destiantionT,fromT,n-1)
print("---0 Disk:---")
hanoiPrintSteps(0,'FromTower','DestinationTower','HelperTower',0)
print("---1 Disk:---")
hanoiPrintSteps(1,'FromTower','DestinationTower','HelperTower',0)
print("---2 Disk:---")
hanoiPrintSteps(2,'FromTower','DestinationTower','HelperTower',0)
print("---3 Disk:---")
hanoiPrintSteps(3,'FromTower','DestinationTower','HelperTower',0)

