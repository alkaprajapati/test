# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def isSymmetric(self, root: TreeNode) -> bool:
        return self.helper(root,root)

    def helper(self, left: TreeNode,right: TreeNode) -> bool:
        if left == None and right == None:
            return True

        if left == None or right == None:
            # print('here')
            return False

        # if  left.val != right.val:
        #     print('yyup')

        return (left.val == right.val) and self.helper(left.left,right.right) and self.helper(left.right,right.left)
