def sumII(nums,target):
    # Two-pointer Approach
    start = 0
    end = len(nums) - 1

    while start <= end:
        total = nums[start] + nums[end]
        if  total == target:
            return [start,end]
        elif total < target:
            start+=1
        else:
            end-=1
    return [-1,-1]

nums = [2, 7, 11, 15]
target = 9
print(sumII(nums,target))
