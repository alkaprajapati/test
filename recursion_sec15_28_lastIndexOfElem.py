# Get the last Index at which the element was found; if not found return -1
# I am using end to start
def lastIndexOfElem(arr,arrLen,elem):
    # End -> Start
    # Base
    if arrLen == -1:
        return -1
    # Assumption + Induction Step
    if elem == arr[arrLen-1]:
        return arrLen-1

    return lastIndexOfElem(arr,arrLen-1,elem)

def lastIndexOfElem(arr,arrLen,elem,startIndex):
    # Start -> End
    # Base
    if arrLen == startIndex:
        return -1
    # Assumption + Induction Step
    if elem == arr[startIndex]:
        return startIndex
    return lastIndexOfElem(arr,arrLen-1,elem)

print(lastIndexOfElem([1,2,3,4,5],5,4))
print(lastIndexOfElem([1,4,3,4,5],5,4))
print(lastIndexOfElem([1,0,3,0,4],5,4))
print(lastIndexOfElem([1,2,3,4,5],5,9))
