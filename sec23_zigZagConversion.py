# The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this:
# (you may want to display this pattern in a fixed font for better legibility)
# P   A   H   N
# A P L S I I G
# Y   I   R
# And then read line by line: "PAHNAPLSIIGYIR"
# Write the code that will take a string and make this conversion given a number of rows:
# string convert(string s, int numRows);
def zigZagConversion(s,numRows):

    if  numRows == 1 or len(s) <= numRows:
        return s

    arr = [''] * numRows
    count =  -1
    for char in s:
        if count >= numRows-1:
            direction = -1
        elif count <= 0:
            direction = 1

        count+=direction
        arr[count] += char

    return ''.join(arr)

print(zigZagConversion("ABCD",2))
print(zigZagConversion('PAYPALISHIRING',1))
print(zigZagConversion('PAY',3))
print(zigZagConversion('PA',3))
print(zigZagConversion('',3))
# # PAHNAPLSIIGYIR
print(zigZagConversion('PAYPALISHIRING',3))
# # PINALSIGYAHRPI
print(zigZagConversion('PAYPALISHIRING',4))
#
