def sumhash(nums,target):
    # Hash Map O(n)
    hashMap = {}
    for idx,value in enumerate(nums):
        diff = target - value
        if diff in hashMap:
            return [hashMap[diff],idx]
        else:
            hashMap[value] = idx

nums = [2, 7, 11, 15]
target = 9
print(sumhash(nums,target))
