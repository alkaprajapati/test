# Find the sum of digits in a number
def multiplication(m,n):
    # Base
    if  n == 0:
        return 0
    # Assumption + Induction Step
    return  m + multiplication(m,n-1)

print(multiplication(9,3))
print(multiplication(9,4))
print(multiplication(11,3))
print(multiplication(11,4))

