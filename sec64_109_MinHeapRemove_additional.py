def upHeapify(elemIdx,parentIdx,minHeap):

    while elemIdx >= 0 and parentIdx>=0 and minHeap[elemIdx] < minHeap[parentIdx]:
        (minHeap[parentIdx],minHeap[elemIdx]) = (minHeap[elemIdx],minHeap[parentIdx])
        elemIdx     = parentIdx
        parentIdx   = (parentIdx - 1)//2

def minHeap(arr):
    minHeap = []
    minHeapLen = 0

    for i in range(len(arr)):
        if i == 0:
            minHeap.append(arr[i])
            minHeapLen+=1
        else:
            parentIdx = (i-1) // 2
            parent = minHeap[parentIdx]
            elem = arr[i]
            elemIdx = i
            minHeap.append(elem)
            minHeapLen+=1
            upHeapify(elemIdx,parentIdx,minHeap)

    return minHeap

arr= [12,6,5,100,1,9,0,14]
print(minHeap(arr))
arr= [12,6,5,100,1]
print(minHeap(arr))
