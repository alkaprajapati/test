# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def sortedArrayToBST(self, nums: List[int]) -> TreeNode:
     
        if nums == []:
            return None        
        
        lenNums = len(nums) -1 
        return self.helper(nums,0,lenNums)
    
    def helper(self,nums,start,end):
        if start > end:
            return
        mid = start + (end - start) // 2
        print(mid)
        root = TreeNode(nums[mid])
        root.left = self.helper(nums,start,mid-1)
        root.right = self.helper(nums,mid+1,end)
        return root
