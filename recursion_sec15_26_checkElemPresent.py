# Check if an element is present in the array or not
# Traversing direction is onto you...I prefer using an index to track iteration for next recursion call
def isElemPresent(arr,arrLen,elem,firstIndex):
    # Base
    if arrLen == firstIndex:
        return False
    # Assumption + Induction Step
    if elem == arr[firstIndex]:
        return True
    else:
        return isElemPresent(arr,arrLen,elem,firstIndex+1)

print(isElemPresent([1,2,3,4,5],5,4,0))
print(isElemPresent([1,2,3,4,5],5,9,0))
