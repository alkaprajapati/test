# Recursion is serious magic :) loving it
def recur_factorial(n):
    # PMI1: BASE CASE
    if n < 0:
        return -1
    elif n == 1 or n == 0:
        return 1
    # PMI2: ASSUMPTION
    smallerSol = recur_factorial(n-1)
    # PMI3: Prove for N+1
    result = n * smallerSol
    return result
print(recur_factorial(1))
print(recur_factorial(2))
print(recur_factorial(3))
print(recur_factorial(4))
print(recur_factorial(10))
print(recur_factorial(-10))
