class HashTable:
    def __init__(self,max):
        self.max = max
        # Array with max None Values
        self.arr = [None for i in range(max)]

    def get_hash1(self, key):
        # This isnt that efficient an hash function
        hash = 0
        for char in key:
            hash += ord(char)
        # print(hash % self.max)
        # % max => hash code will be between 0 and max.
        # this is compression
        return hash % self.max

    def get_hash(self, key):
        hash = 0
        p = 37
        base = 1
        for char in key:
            hash += ord(char) * base
            base = ( base * 37 ) % self.max
        # print(hash % self.max)
        return hash % self.max

    def __getitem__(self, index):
        # __getitem__ is python's standard operator
        hashIndex = self.get_hash(index)
        return self.arr[hashIndex]

    def __setitem__(self, key, val):
        #__setitem__ is python's standard operator
        hashIndex = self.get_hash(key)
        self.arr[hashIndex] = val

    def __delitem__(self, key):
        # __delitem__ is python's standard operator
        hashIndex = self.get_hash(key)
        self.arr[hashIndex] = None

hashTableIns = HashTable(50)
hashTableIns["ke2"] = 19
hashTableIns["key1"] = 39
hashTableIns["key2"] = 40
hashTableIns["key3"] = 41
hashTableIns["k4ey"] = 42
hashTableIns["key4"] = 42
print(hashTableIns.arr)
print(hashTableIns["key4"])
del hashTableIns["key3"]
print(hashTableIns.arr)
