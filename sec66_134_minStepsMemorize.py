def helper(n,ans):
    if n <= 1:
        return 0
    # print(n)
    if ans[n] != -1:
        return ans[n]

    # decrement by 1
    opt1 = helper(n-1,ans)
    # Divide by 2
    opt2 = float('inf')
    if n % 2 == 0:
        opt2 = helper(n//2,ans)
    # Divide by 3
    opt3 = float('inf')
    if n % 3 == 0:
        opt3 = helper(n//3,ans)

    ans[n] = min(opt1,opt2,opt3)+1
    return ans[n]

def minStepsMemorize(n):

    ans = [-1]*(n+1)
    return  helper(n,ans)


# print(minStepsMemorize(1))
# print(minStepsMemorize(2))
# print(minStepsMemorize(3))
# print(minStepsMemorize(4))
print(minStepsMemorize(7))
print(minStepsMemorize(11))
