# Print all Subsequent strings for a given string
# Include and exclude every character once from the current string
def subsequenceStrings(str,out,arr):
    if  len(str) == 0:
        arr.add(out)
        return

    # exclude the character from output string
    subsequenceStrings(str[1:],out,arr)
    # include the character from output string
    subsequenceStrings(str[1:],out+str[0],arr)

arr = set()
subsequenceStrings('abc','',arr)
print(arr)

