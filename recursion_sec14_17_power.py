def recur_power(num,n):
    # Base
    if  n == 0:
        return 1
    elif  n == 1:
        return num

    # Assumption + Induction Step
    return num * recur_power(num, n-1)

print(recur_power(2,0))
print(recur_power(2,1))
print(recur_power(2,4))
print(recur_power(2,8))
print(2**8)
print(recur_power(2,15))
print(2**15)
