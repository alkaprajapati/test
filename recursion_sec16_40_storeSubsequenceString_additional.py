# Print all Subsequent strings for a given string
# Include and exclude every character once from the current string
def subsequenceStrings(str,start,end,out,arr):
    if start > end:
        return
    print(start,end)
    arr.add(out)
    arr.add(out+str[start])
    # exclude the character from output string
    subsequenceStrings(str,start+1,end,out,arr)
    # include the character from output string
    subsequenceStrings(str,start+1,end,out+str[start],arr)

arr = set()
subsequenceStrings('abc',0,2,'',arr)
print(arr)

