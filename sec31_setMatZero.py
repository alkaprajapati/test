def setMatrixZero(matrix):

    rows = len(matrix)

    if rows == 0: return []

    firstRow = firstCol = -1
    cols = len(matrix[0])

    if matrix[0][0]==0:
        firstRow=firstCol=0

    for row in range(rows):
        for col in range(cols):
            if matrix[row][col] == 0:
                if row == 0:
                    firstRow=0
                elif col == 0:
                    firstCol=0
                matrix[row][0] = 0
                matrix[0][col] = 0

    for row in range(1,rows):
        for col in range(1,cols):
            if matrix[0][col] == 0 or matrix[row][0] == 0:
                matrix[row][col] = 0

    if firstRow == 0:
        for col in range(cols):
            matrix[0][col] = 0
    if firstCol == 0:
        for row in range(rows):
            matrix[row][0] = 0

    return matrix

matrix = [[1,1,1],[1,0,1],[1,1,1]]
print(setMatrixZero(matrix))
matrix = [[0,1,2,0],[3,4,5,2],[1,3,1,5]]
print(setMatrixZero(matrix))
matrix = [[0,1]]
print(setMatrixZero(matrix))
matrix =[[1,1,1],[0,1,2]]
print(setMatrixZero(matrix))
