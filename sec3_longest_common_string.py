def longestCommonPrefix1(strs):
    # Note that complexity wise it may not much differ with implementation below,
    # but makes a diff performance wise
    totalStr = len(strs)

    if totalStr == 0 or "" in strs:
        return ""
    elif totalStr == 1:
        return strs[0]

    s.sort()
    firstWord = strs[0]
    toIndex = len(strs[0])

    for i in range(0,toIndex):
        for j in range(1,totalStr):
            if firstWord[i] != strs[j][i]:
                return strs[j][0:i]

    return strs[0]

def longestCommonPrefix(strs):
        totalStr = len(strs)

        if totalStr == 0 or "" in strs:
            return ""
        elif totalStr == 1:
            return strs[0]

        firstWord = strs[0]
        toIndex = len(strs[0])

        for i in range(0,toIndex):
            for j in range(1,totalStr):
                if i >= len( strs[j] ) or firstWord[i] != strs[j][i]:
                    return strs[j][0:i]

        return strs[0]

s = [""]
print("common string is",longestCommonPrefix(s))
s = ["b"]
print("common string is",longestCommonPrefix(s))
s = ["a","a"]
print("common string is",longestCommonPrefix(s))
s = ["aaa","aaaa","aaaaa"]
print("common string is",longestCommonPrefix(s))
s = ["flower","flower"]
print("common string is",longestCommonPrefix(s))
s = ["flower","flower2","flower23"]
print("common string is",longestCommonPrefix(s))
s = ["flower23","flower2","flower"]
print("common string is",longestCommonPrefix(s))
s = ["abbb","a","accc","aa"]
print("common string is",longestCommonPrefix(s))