# This algo looks very simple but this is the base to understanding backtracking in recursion
# Print all permutations of a string
# ABC -> ABC BAC CBA ABC BCA ACB
def stringPermutation(str,lenStr,i):
    # Base case return the empty case
    if i == lenStr:
        print(''.join(str))
        return

    for j in range(i,lenStr):
        (str[i],str[j]) = (str[j],str[i])
        stringPermutation(str,lenStr,i+1)
        # Below step is important as we need to keep
        # swapping places in the ORIGINAL String
        (str[i],str[j]) = (str[j],str[i])

stringPermutation(list('abc'),3,0)
