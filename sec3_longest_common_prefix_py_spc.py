def longestCommonPrefix(strs):
    totalStr = len(strs)

    if totalStr == 0 or "" in strs:
        return ""
    elif totalStr == 1:
        return strs[0]
    allMatched = False
    toIndex  = len(strs[0])

    strs.sort()

    for i in range(0,len(strs[0])):
        for j in range(0,totalStr - 1):
            if strs[j][i] != strs[j+1][i]:
                return strs[j][0:i]
            else:
                allMatched = True

    if  allMatched == True:
        return strs[0][0:i+1]

s = [""]
print("common string is",longestCommonPrefix(s))
s = ["b"]
print("common string is",longestCommonPrefix(s))
s = ["a","a"]
print("common string is",longestCommonPrefix(s))
s = ["aaa","aaaa","aaaaa"]
print("common string is",longestCommonPrefix(s))
s = ["flower","flower"]
print("common string is",longestCommonPrefix(s))
s = ["flower","flower2","flower23"]
print("common string is",longestCommonPrefix(s))
s = ["flower23","flower2","flower"]
print("common string is",longestCommonPrefix(s))
s = ["abbb","a","accc","aa"]
print("common string is",longestCommonPrefix(s))

