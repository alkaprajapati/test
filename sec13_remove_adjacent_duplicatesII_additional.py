# This code I got from solutions on leetcode 96% faster
def remAdjacentDuplicates(s,k):

    while (True):

        lenBefore = len(s)
        for ch in set(s):
            s = s.replace(ch*k,'')

        if len(s) == lenBefore:
            break

    return s

remAdjacentDuplicates('saasa',2)
# remAdjacentDuplicates('saaassa',3)
