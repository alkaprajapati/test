# https://www.youtube.com/watch?v=NobHlGUjV3g&list=PL2_aWCzGMAwI3W_JlcBbtYTwiQSsOTa6P&index=3 to understand what a linked list is
# https://www.youtube.com/watch?v=qp8u-frRAnU&list=PLeo1K3hjS3uu_n_a__MI_KktGTLYopZ12&index=4
# Issues with Array that Linked Link overcomes:
# Array is a contiguous block of memory. In python, we only have dynamic array so it block an initial memory with n capacity. After this capacity is exhausted,
# it increases like geometric progression. Now suppose the initial block is exhausted, however the next contiguous memory unit is occupied. Then, the entire
# array is copied to a new memory location, with next contiguous block allocated (amortization-periodic pay off).
# With Linked list individual values are stored in random memory locations that are linked together via pointer. Its benefits are that no pre-allocation of
# memory block needed. And insertion is easy.
#                                        Array:             Linked List
# INDEXING                              O(1)                O(n)
# INSERT/DELETE ELEMENT AT START        O(n)                O(1)
# INSERT/DELETE ELEMENT AT END          O(1)-amortised      O(n)    //Amortised-> if next
# INSERT ELEMENT IN BETWEEN             O(n)                O(n)
# Linked list -> traversal -> O(n)
# Head element points to the first element of the linked list
class ListNode:
    def __init__(self,val=None, next=None):
        self.val = val
        self.next = next

class LinkedList:
    def __init__(self):
        # Instance attr
        self.head = None

    def is_empty(self):
        if self.head is None:
            return True

    def length(self):
        itr=self.head
        count = 0
        while itr:
            count+=1
            itr=itr.next
        return count

    def insert_at_beginning(self,val):
        #  Point to what head is pointing to
        node = ListNode(val,self.head)
        # adjust head to point at current node
        self.head=node

    def insert_at_end(self,val):
        if self.is_empty():
            self.head = ListNode(val,None)
            return
        itr = self.head
        while itr.next:
            itr=itr.next
        itr.next = ListNode(val,None)

    def insert_new_list(self, list):
        self.head = None
        for item in list:
            self.insert_at_end(item)

    def remove_at(self,index):
        if index < 0 or index > self.length()-1:
            raise Exception('Invalid Index')

        if index==0:
            self.head=self.head.next
            return

        itr=self.head
        count = 0
        while itr:
            if count == index - 1:
                itr.next = itr.next.next
                return
            itr=itr.next
            count+=1

    def insert_at(self,index,val):
        if index < 0 or index > self.length():
                raise Exception('Invalid Index')

        if index==0:
            self.insert_at_beginning(self,val)
            return

        count = 0
        itr = self.head
        while itr:
            if count == index - 1:
                node = ListNode(val,itr.next)
                itr.next = node
                return
            itr=itr.next
            count+=1

    def print(self):
        if self.is_empty():
            print('Linked List is empty.')
            return
        itr = self.head
        items = ''
        while itr:
            items += str(itr.val) + '->'
            itr = itr.next
        print(items)

# linkedListObj = LinkedList()
# linkedListObj.print()
# linkedListObj.insert_at_beginning(5)
# # linkedListObj.head()#instance attr hence error
# linkedListObj.print()
# linkedListObj.insert_at_beginning(10)
# linkedListObj.print()
# linkedListObj.insert_at_end(15)
# linkedListObj.print()
# linkedListObj.insert_at_end(20)
# linkedListObj.print()
# print(linkedListObj.length())
# linkedListObj.insert_new_list(['moana','babbit','hulk','gohan'])
# linkedListObj.print()
# # linkedListObj.remove_at(5)
# linkedListObj.remove_at(2)
# linkedListObj.print()
# linkedListObj.insert_at(2,'tigress')
# linkedListObj.print()
# linkedListObj.insert_at(4,'poo')
# linkedListObj.print()
# # linkedListObj.insert_at(7,'poo')
