# Given an input string, reverse the string word by word.
# Input: "the sky is blue"
# Output: "blue is sky the"
def reverseWords(s):
    # Python speciif 80%
    arr = s.split()
    return ' '.join(arr[::-1])

print(reverseWords('the sky is blue'))
print(reverseWords(' Hi There '))
print(reverseWords('     Hi      There   '))
print(reverseWords('H'))
