def spiralOrderMatrix(matrix):

    rowTop,colRight = 0,0
    rowBottom=rows = len(matrix)-1

    if rowBottom == -1:
        return []
    elif rowBottom ==0:
        return matrix[0]

    colLeft=cols = len(matrix[0])-1

    # print(rowTop,rowBottom,colLeft,colRight)
    output=[]
    while rowTop <= rowBottom and colRight <= colLeft:
        # Print top row
        for i in range(colRight,colLeft+1):
            output.append(matrix[rowTop][i])
        # Print Left Col
        for i in range(rowTop+1,rowBottom):
            output.append(matrix[i][colLeft])
        # Print bottom row
        if  rowTop!=rowBottom and colRight-1 >=-1:
            for i in range(colLeft,colRight-1,-1):
                output.append(matrix[rowBottom][i])
        # Print Right Col
        if colLeft!=colRight and rowBottom-1 >=0:
            for i in range(rowBottom-1,rowTop,-1):
                output.append(matrix[i][colRight])
        rowTop      +=1
        colLeft     -=1
        rowBottom   -=1
        colRight     +=1

    return output

matrix= [[1],[2],[3],[4],[5],[6],[7],[8],[9],[10]]
print(spiralOrderMatrix(matrix))
matrix = [[1,2,3]]
print(spiralOrderMatrix(matrix))
matrix = [[1,2,3],[4,5,6],[7,8,9]]
print(spiralOrderMatrix(matrix))
matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
print(spiralOrderMatrix(matrix))
matrix=[[1, 2, 3, 4,5],[6, 7, 8, 9,10],[11,12,13,14,15],[16,17,18,19,20]]
print(spiralOrderMatrix(matrix))
