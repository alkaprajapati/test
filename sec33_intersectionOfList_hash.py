# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None
# O(n) S(n)
class Solution:
    def getIntersectionNode(self, headA: ListNode, headB: ListNode) -> ListNode:
        if not (headA and headB):
            return None
        hashMap = {}
        while headA != None:
            hashMap[headA] = True
            headA = headA.next

        while headB != None:
            if headB in hashMap.keys():
                return headB
            headB = headB.next
        return None
