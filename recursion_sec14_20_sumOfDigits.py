# Find the sum of digits in a number
def sumOfDigits(n):
    # Base
    if  n == 0:
        return 0
    # Assumption + Induction Step (add remainder -> last digit)
    return  (n % 10) + sumOfDigits(n//10)

print(sumOfDigits(9))
print(sumOfDigits(12))
print(sumOfDigits(100))
print(sumOfDigits(109))
print(sumOfDigits(143))
print(sumOfDigits(1435))
print(sumOfDigits(14355))
print(sumOfDigits(143555))
