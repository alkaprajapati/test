def mergeArray(arr,start,end,mid):
    # Not we not using two different arrays
    # We using the actual array with the mentioned start and end
    i,j = start,mid+1

    result = []

    # Compare the element of both halves
    while  i <= mid and j <= end:
        if arr[i] < arr[j]:
            result.append(arr[i])
            i += 1
        else:
            result.append(arr[j])
            j += 1

    # This part if important in cases when the halves are of different length
    # We copy rest of the elements in bigger array. E.g. A[2,5,9] B[3,6]
    # A[9] will be left out from above comparison

    if i <= mid:
        result += arr[i:mid+1]

    if j <= end:
        result += arr[j:end+1]

    # Copy the sorted result back to original array
    arr[start:end+1] = result

def mergeSort(arr,start,end):
    if start >= end:
        return

    mid = ( start + end ) // 2

    # Sort First half of the array
    mergeSort(arr,start,mid)
    # Sort second half of the array
    mergeSort(arr,mid+1,end)
    # Merge the sorted arrays
    mergeArray(arr,start,end,mid)

    return

arr = [7,5,1,2,1,3]
mergeSort(arr,0,5)
print(arr)

arr = [7,5,1,2,4,3]
mergeSort(arr,0,5)
print(arr)
