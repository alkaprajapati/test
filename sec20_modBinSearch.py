# Suppose an array sorted in ascending order is rotated at some pivot unknown to you beforehand.
# (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).
# You are given a target value to search. If found in the array return its index, otherwise return -1.
# You may assume no duplicate exists in the array.
# Your algorithm's runtime complexity must be in the order of O(log n).
def searchRotatedArr(nums,target):
    if nums == []:
        return - 1

    low = 0
    high = len(nums) - 1
    while low <= high:
        mid = (low + high) // 2
        # print(mid,low,high)
        if nums[mid] == target:
            return mid
        # If left side is sorted
        elif nums[low] <= nums[mid]:
            # print('left')
            if target < nums[mid] and target >=nums[low]:
                high = mid - 1
            else:
                low = mid + 1
        # if Right side is sorted
        elif nums[mid] <= nums[high]:
            # print('right')
            if  target > nums[mid] and target <=nums[high]:
                low = mid + 1
            else:
                high = mid - 1
    return -1

nums = []
print(searchRotatedArr(nums,0))
nums = [4,5]
print(searchRotatedArr(nums,0))
nums = [5,4]
print(searchRotatedArr(nums,4))
nums = [5,4]
print(searchRotatedArr(nums,5))
nums = [4,5,6,7,0,1,2]
print(searchRotatedArr(nums,0))
nums = [2,4,5,6,7,0,1]
print(searchRotatedArr(nums,0))
nums = [7,0,1,2,3,4,5]
print(searchRotatedArr(nums,0))
nums = [7,5,6,0,1,2,4]
print(searchRotatedArr(nums,0))
