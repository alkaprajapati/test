# single line comprehension of if else
for i in range(4): print(i if i<2 else 0)

# assign elements to a 1D array
squares = [i**2 for i in range(10)]
print(squares)

# assign elements to a 1D array
squares = [i**2 for i in range(10)]
print(squares)

# Nested loop comprehension
for i in range(3):
    for j in range(2):
        print(i,j)
#         There is no way to comprehend it standalone
# for i in range(3): for j in range(2): print(i,j)
# But we can use comprehension to save result in an aray or print it
print((i,j) for i in range(3) for j in range(3))
print([(i,j) for i in range(3) for j in range(3)])
arr= [(i,j) for i in range(3) for j in range(3)]
print(arr)


