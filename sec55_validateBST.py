# Given a binary tree, determine if it is a valid binary search tree (BST).
# Assume a BST is defined as follows:
# The left subtree of a node contains only nodes with keys less than the node's key.
# The right subtree of a node contains only nodes with keys greater than the node's key.
# Both the left and right subtrees must also be binary search trees.
# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
class Solution:

    def isValidBST(self, root: TreeNode) -> bool:

        rangeStart = -float('inf')
        rangeEnd   = float('inf')
        return self.helper(root,rangeStart,rangeEnd)

    def helper(self, root,rangeStart,rangeEnd) -> bool:
        if root == None:
            return True
        # root value should between the range
        validRoot = (root.val > rangeStart and root.val < rangeEnd)
        # for left tree the range will be at max the root.val (not inclusive)
        if validRoot:
            validLeft = self.helper(root.left,rangeStart,root.val)
        # for right tree the range will be min from root.val (not inclusive) to inf
            validRight = self.helper(root.right,root.val,rangeEnd)
            return validLeft and validRight
        else:
            return False
