dictDemo = {}

# assigning key value pair
dictDemo["key1"] = "value1"
dictDemo["key2"] = "value2"
print(dictDemo)

# Auto fill keys from a string
strKey = "abcde"
dictDemo1 = {}
dictDemo1=dictDemo1.fromkeys(strKey)
print(dictDemo1)

# Auto fill keys from an array
arrKey = ['key1','key2','key3']
dictDemo2 = {}
dictDemo2=dictDemo2.fromkeys(arrKey)
print(dictDemo2)

#accessing a value
print(dictDemo["key1"])

#getting all keys
print("#getting all keys")
print(dictDemo.keys())

#getting all values
print("#getting all values")
print(dictDemo.values())

#getting all key-value pair
print("#getting all key-value pair")
print(dictDemo.items())

# get the value of a key, but if key is not found set the key with given default value
value = dictDemo.setdefault("key6","value6")
print(value,dictDemo)

# del item with specified key
dictDemo.pop("key6")
print(dictDemo)
