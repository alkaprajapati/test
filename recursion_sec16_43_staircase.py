# Find the number of ways to reach N from 0
# Given we can take a jump of 1,2,3
def staircase(n):
    #Base
    if n == 1:
        return 1
    elif n == 2:
        return 2
    elif n == 3:
        return 4
    #Assumption + Induction step
    return staircase(n-1) + staircase(n-2) + staircase(n-3)

def staircase1(n):
    #Base
    # In this we not hardcoding the base case
    if n < 0:
        return 0
    elif n == 0 or n == 1:
        return 1

    #Assumption + Induction step
    return staircase1(n-1) + staircase1(n-2) + staircase1(n-3)

print(staircase1(1))
print(staircase1(2))
print(staircase1(3))
print(staircase1(4))
print(staircase1(5))
print(staircase1(6))
print(staircase1(7))
print(staircase1(8))
print(staircase1(9))

print(staircase(1))
print(staircase(2))
print(staircase(3))
print(staircase(4))
print(staircase(5))
print(staircase(6))
print(staircase(7))
print(staircase(8))
print(staircase(9))
