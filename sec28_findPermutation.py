# Given a positive integer B and a string A
# string consisting of only two letters D or I
# find any permutation of n positive integer (1 to n) that satisfy the given input string
# B will be len(A) + 1
def findPermutation(A,B):
    # T(n) S(1)
    smallest = 1
    largest  = B
    arr = []
    for i in range(0,B-1):
        char = A[i]
        if char == 'I':
            arr.append(smallest)
            smallest += 1
        else:
            arr.append(largest)
            largest -= 1
    arr.append(smallest)
    print(arr)

findPermutation('I',2)
findPermutation('D',2)
findPermutation('ID',3)
findPermutation('DI',3)
findPermutation('II',3)
findPermutation('DD',3)
findPermutation('III',4)
findPermutation('IID',4)
findPermutation('IDI',4)
findPermutation('DII',4)
findPermutation('DDD',4)

