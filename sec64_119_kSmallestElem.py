import sec64_110_maxPriorityQ_additional as maxhp

def kSmalledElem(arr,k):

    maxPQ = maxhp.MaxPriorityQueue()

    # Push first K elements in to max heap Q
    for i in range(k):
        # k log k
        maxPQ.insert(arr[i])

    # Initialize pointers
    i = k

    while i<len(arr):
        # 2(n-k) log k
        max = maxPQ.getMax()
        curr = arr[i]
        if curr < max:
            maxPQ.removeMax()
            maxPQ.insert(curr)
        i+=1

    #
    while not maxPQ.isEmpty():
        # k log k
        print(maxPQ.removeMax())

arr = [10,15,6,4,5]
kSmalledElem(arr,2)
print('---')
arr = [5,6,9,12,3,13,2]
kSmalledElem(arr,3)
print('---')
arr = [8,5,12,10,0,1,6,9]
kSmalledElem(arr,4)
