# Print all the position of the Element in a given List; Print nothing if not found
def storeAllPos(arr,arrLen,elem,positions):
    # End -> Start
    # Base
    if arrLen == 0:
        return

    storeAllPos(arr,arrLen-1,elem,positions)

    # Assumption + Induction Step
    if elem == arr[arrLen-1]:
        positions.append(arrLen-1)

def print_result(arr):
    for i in arr:
        print(i)

print("Case1:")
positions = []
storeAllPos([1,2,3,4,5],5,4,positions)
print_result(positions)
print("Case2:")
positions = []
storeAllPos([1,4,3,4,5],5,4,positions)
print_result(positions)
print("Case3:")
positions = []
storeAllPos([1,0,8,0,0],5,0,positions)
print_result(positions)
print("Case4:")
positions = []
storeAllPos([1,2,5,5,5],5,9,positions)
print_result(positions)
