# Print the count of occurrence of an element
def countOccurrence(arr,arrLen,elem):
    # End -> Start
    # Base
    if arrLen == 0:
            return 0

    # Assumption + Induction Step
    if elem == arr[arrLen-1]:
        return 1 + countOccurrence(arr,arrLen-1,elem)
    else:
        return 0 + countOccurrence(arr,arrLen-1,elem)

print("Case1:")
print(countOccurrence([1,2,3,4,5],5,4))
print("Case2:")
print(countOccurrence([1,4,3,4,5],5,4))
print("Case3:")
print(countOccurrence([1,0,0,6,0],5,0))
print("Case4:")
print(countOccurrence([1,2,5,5,5],5,9))
